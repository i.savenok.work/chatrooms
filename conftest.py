import pytest

pytest_plugins = [
    'common.tests.fixtures',
    'entities.user.tests.fixtures',
    'entities.room.tests.fixtures',
]


@pytest.fixture(autouse=True)
def clear_store():
    yield
    from entities.room.store import ROOMS_STORE
    from entities.user.store import USERS_STORE
    ROOMS_STORE.clear()
    USERS_STORE.clear()


@pytest.fixture(autouse=True)
def set_test_mode():
    import settings
    settings.TEST_MODE = True
