import {AUTH, LOCAL_STORAGE, USE_EFFECTS, WS} from "./common/loggers";  // do not remove


function getEnv(name: string, defaultValue: string|undefined = undefined) {
    if (process.env[name]) {
        if (["none", "null", "", "\"\""].includes(process.env[name]!)) {
            return undefined
        }
        return process.env[name]
    } else {
        return defaultValue
    }
}


export class Env {
    static API_PREFIX = getEnv('REACT_APP_API_PREFIX', '/api/v1')
    static API_HOST = getEnv('REACT_APP_API_HOST', 'http://localhost')
    static API_PORT = getEnv('REACT_APP_API_PORT', '8000')

    static WS_PREFIX = getEnv('REACT_APP_WS_PREFIX', '/ws/')
    static WS_HOST = getEnv('REACT_APP_WS_HOST', 'ws://localhost');
    static WS_PORT = getEnv('REACT_APP_WS_PORT', '5678')
    static ENABLED_LOGGERS = [
        "",
        // WS,
        // LOCAL_STORAGE,
        // AUTH,
        // USE_EFFECTS
    ];
}
