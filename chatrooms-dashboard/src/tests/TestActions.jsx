import {User} from "../common/user";
import {ApiClient} from "../common/apiClient";
import {WSEvents} from "../common/ws/wsEvents";
import React from "react";


export function Test() {
    let user1  = null

    const createUser1 = () => {
        ApiClient.signInSignUp(
        (d) => {user1 = new User(d.id, d.name, d.color, d.token)},
        () => {},
        '1',
        'pas',
        '#cbf542',
    )
    }
    let ws1 = null
    const connect1 = () => {ws1 = new WSEvents(user1)}

    let user2 = null

    const createUser2 = () => {
        ApiClient.signInSignUp(
        (d) => {user2 = new User(d.id, d.name, d.color, d.token)},
        () => {},
        '2',
        'pas',
        '#cbf542',
    )
    }

    const connect2 = () => {const ws2 = new WSEvents(user2)}
    const disconnect1 = () => {ws1?.ws.close()}
    let room = null;
    const createRoom = () => {ApiClient.createRoom(
        (d) => {room = d},
        (e) => {},
        'room'
    )}

    return (

        <div>
            <div onClick={(e) => {createUser1()}}>create 1</div>
            <div onClick={(e) => {createUser2()}}>create 2</div>
            <div onClick={(e) => {connect1()}}>connect 1</div>
            <div onClick={(e) => {connect2()}}>connect 2</div>
            <div onClick={(e) => {disconnect1()}}>disconnect 1</div>
            <div onClick={(e) => {createRoom()}}>create room</div>

            <div onClick={(e) => {
                ws1.sendUserEnteredRoom({room_id: room.id})
            }}>notify user1 entered room</div>
            <div onClick={(e) => {
                ws1.sendUserLeftRoom()
            }}>notify user1 left room</div>

        </div>
    )
}
