import styled from "styled-components";
import {__getStylesState__} from "./common/globals";


export const AppWrapper = styled.div`
    background-color: ${__getStylesState__().commonFirstBackgroundColor};
    color: ${__getStylesState__().commonFirstColor};
    height: auto;
    min-height: 100vh;
`
