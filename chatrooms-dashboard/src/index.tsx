import React, {useEffect, useState} from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {SignInSignUpPage} from "./components/SignInSignUpPage/SignInSignUpPage";
import {LocalStorageClient} from "./common/localStorageClient";
import {ApiClient} from "./common/apiClient";
import {User} from "./common/user";
import {RoomsListPage} from "./components/RoomsListPage/RoomsListPage";
import {use_effect_log__} from "./common/loggers";
import {Provider} from "react-redux";
import {__getStore__, __setCurrentUser__, __unsetCurrentUser__} from "./common/globals";
import {userResponseToUserObject} from "./common/transformators";
import {LoadingWidget} from "./components/common/LoadingWidget/LoadingWidget";
import {ProfileWidget} from "./components/common/ProfileWidget/ProfileWidget";
import {AppWrapper} from "./styled";


function App() {
    console.log('...rebuilding app...')
    const [currentUser, _setCurrentUser] = useState<User|null>(null);
    const [isStoredProfileChecked, setProfileChecked] = useState(false)

    const setCurrentUser = (user: User) => {__setCurrentUser__(user); _setCurrentUser(user)}
    const unsetCurrentUser = () => {__unsetCurrentUser__(); _setCurrentUser(null)}

    const tryToLoadStoringProfile = (storingToken: string) => {
        ApiClient.getProfileInfo(
            (userData) => {
                setCurrentUser(userResponseToUserObject(userData));
                setProfileChecked(true);
            },
            (error) => {
                unsetCurrentUser();
                setProfileChecked(true);
            },
            storingToken,
        )
    }

    useEffect(() => {
        use_effect_log__('App, []')
        const storingUserToken = LocalStorageClient.getToken()
        if (!currentUser && storingUserToken) {
            tryToLoadStoringProfile(storingUserToken)
        } else {
            setProfileChecked(true);
        }
    }, [])

    if (currentUser?.token && currentUser?.id) {
        return (
            <>
                <ProfileWidget onLogOutClicked={unsetCurrentUser} />
                <RoomsListPage />
            </>
        )
    } else if (isStoredProfileChecked) {
        return <SignInSignUpPage userSetter={setCurrentUser}/>
    } else {
        return <LoadingWidget active={true} />
    }
}


ReactDOM.render(
    <Provider store={__getStore__()}>
        <AppWrapper>
            <App />
        </AppWrapper>
    </Provider>,
    document.getElementById('root')
);
