import {
    WSEventMessageCreatedDataType,
    WSEventRoomCreatedDataType,
    WSEventUserEnteredRoomDataType, WSEventUserLeftRoomDataType
} from "./wsEvents";
import {ActionFactory} from "../store/store";
import {messageWsToMessage, roomWsToRoom} from "../transformators";
import {__getStore__} from "../globals";
import {WSEventsTypes} from "./wsEventsTypes";


export const defaultWSEventsHandlers = {
    [WSEventsTypes.EVENT_ROOM_CREATED]: (data: WSEventRoomCreatedDataType) => {
        __getStore__().dispatch(ActionFactory.addRoom(roomWsToRoom(data)))
    },

    [WSEventsTypes.EVENT_MESSAGE_CREATED]: (data: WSEventMessageCreatedDataType) => {
        __getStore__().dispatch(ActionFactory.addRoomMsg(messageWsToMessage(data)))
    },

    [WSEventsTypes.EVENT_USER_ENTERED_ROOM]: (data: WSEventUserEnteredRoomDataType) => {
        __getStore__().dispatch(ActionFactory.setUserConnectedToRoom(data.user.id, data.room_id))
        __getStore__().dispatch(ActionFactory.addUser(data.user))
    },

    [WSEventsTypes.EVENT_USER_LEFT_ROOM]: (data: WSEventUserLeftRoomDataType) => {
        __getStore__().dispatch(ActionFactory.unsetUserConnectedToRoom(data.user_id, data.room_id))
    }
}
