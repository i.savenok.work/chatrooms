import {User} from "../user";
import {ws_log__} from "../loggers";
import {Env} from "../../settings";
import {UserResponseType} from "../apiClient";
import {defaultWSEventsHandlers} from "./defaultHandlers";
import {WSEventsTypes} from "./wsEventsTypes";


export class WSEvents {
    WS_ADDRESS = `${Env.WS_HOST}${Env.WS_PORT ? ':'+Env.WS_PORT : ''}${Env.WS_PREFIX}`;

    ws: WebSocket
    handlers : { [key: string]: {(data: any): void} }

    constructor(user: User) {
        ws_log__(`Connecting as ${user.name}`)
        this.ws = new WebSocket(`${this.WS_ADDRESS}?token=${user.token}`)
        this.handlers = defaultWSEventsHandlers
        this.ws.onmessage = (event) => {
            const message: any = JSON.parse(event.data);
            ws_log__(`New msg for ${user.name}, event: ${message.event}`)
            const handler = this.handlers[message?.event]
            if (handler) {
                handler(message.data)}
        }
        this.ws.onclose = (event => {ws_log__(`${user.name} disconnected`)})
    }

    setHandler(
        eventType: string,
        handler: {(data: any): void}
    ) {this.handlers[eventType] = handler}

    _sendMsg(event: string, data: object | null) {
        ws_log__(`sending ${event}`)
        this.ws.send(JSON.stringify({event: event, data: data ? data : {}}))
    }

    sendUserEnteredRoom(data: ClientWSEventUserEnteredRoomDataType) {
        this._sendMsg(WSEventsTypes.CLIENT_EVENT_USER_ENTERED_ROOM, data)
    }
    sendUserLeftRoom() {
        this._sendMsg(WSEventsTypes.CLIENT_EVENT_USER_LEFT_ROOM, null)
    }

}


export type WSEventRoomCreatedDataType = { id: string, name: string }
export type WSEventMessageCreatedDataType = {
    id: string,
    room_id: string,
    user_id: string,
    user_name: string,
    order: number,
    content: string,
}
export type WSEventUserEnteredRoomDataType = { user: UserResponseType, room_id: string }
export type WSEventUserLeftRoomDataType = { user_id: string, room_id: string }

export type ClientWSEventUserEnteredRoomDataType = { room_id: string }
