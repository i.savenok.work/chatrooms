

export class WSEventsTypes {
    static EVENT_ROOM_CREATED = 'room.created';
    static EVENT_MESSAGE_CREATED = 'msg.created';
    static EVENT_USER_ENTERED_ROOM = 'user.entered_room';
    static EVENT_USER_LEFT_ROOM = 'user.left_room';

    static CLIENT_EVENT_USER_ENTERED_ROOM = 'user.entered_room';
    static CLIENT_EVENT_USER_LEFT_ROOM = 'user.left_room';

}
