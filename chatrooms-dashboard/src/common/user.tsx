

export class User {
    id: string = '';
    name: string = '';
    color: string = '';
    token: string = '';
    constructor(id: string, name: string, color: string, token: string) {
        this.id = id
        this.name = name
        this.color = color
        this.token = token
        this.getAuthHeader = this.getAuthHeader.bind(this)
    }

    getAuthHeader(): {Authorization: string} {return {"Authorization": "JWT " + this.token}}

}
