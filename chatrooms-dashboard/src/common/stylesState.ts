

export type StylesStateType = {
    commonFirstBackgroundColor: string;
    commonSecondBackgroundColor: string;
    commonThirdBackgroundColor: string;
    commonHoverBackgroundColor: string;
    commonActivatedBackgroundColor: string;
    commonFirstColor: string;
    commonSecondColor: string;
}

export const defaultStylesState = {
    commonFirstBackgroundColor: "#452a73",  // global background
    commonSecondBackgroundColor: "#e3f0ff",  // active elements background
    commonThirdBackgroundColor: "#b7a9d4",  // live elements background

    commonHoverBackgroundColor: "#964182",  // live elements on hover
    commonActivatedBackgroundColor: "#d43386",  // live elements activated

    commonFirstColor: "#3e2e5e",  // text color
    commonSecondColor: "#8e82a8",  // additional text color
}
