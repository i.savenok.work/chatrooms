import {WSEventMessageCreatedDataType, WSEventRoomCreatedDataType} from "./ws/wsEvents";
import {MessageType, RoomType, UserType} from "./store/store";
import {MessageResponseType, RoomResponseType, UserPrivateResponseType, UserResponseType} from "./apiClient";
import {User} from "./user";


export const roomWsToRoom = (data: WSEventRoomCreatedDataType): RoomType => {return {
    id: data.id,
    name: data.name,
    messages: {},
}}
export const roomResponseToRoom = (data: RoomResponseType): RoomType => {return{
    id: data.id,
    name: data.name,
    messages: {},
}}
export const messageWsToMessage = (data: WSEventMessageCreatedDataType): MessageType => {return {
    id: data.id,
    room_id: data.room_id,
    user_id: data.user_id,
    user_name: data.user_name,
    content: data.content,
}}
export const messageResponseToMessage = (data: MessageResponseType): MessageType => {return {
    id: data.id,
    room_id: data.room_id,
    user_id: data.user_id,
    user_name: data.user_name,
    content: data.content,
}}
export const userResponseToUser = (data: UserResponseType): UserType => {return {
    id: data.id,
    name: data.name,
    color: data.color,
}}
export const userResponseToUserObject = (data: UserPrivateResponseType): User => {return new User(
    data.id,
    data.name,
    data.color,
    data.token,
)}
