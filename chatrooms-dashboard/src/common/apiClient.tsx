import {User} from "./user";
import {Env} from "../settings";
import {__getCurrentUser__} from "./globals";


class Method {
    static GET = "GET"
    static POST = "POST"
}


class Request {
    path: string
    method: string
    user: User | null
    data: object | null
    onSuccess: any
    onError: ErrorCallbackType

    constructor(path: string, method: string | null = null) {
        this.path = path
        this.method = method ? method : Method.GET
        this.user = null
        this.data = null
        this.onSuccess = null
        this.onError = null
    }

    _onSuccess(data: object) {if (this.onSuccess) {this.onSuccess((data))}}
    _onError(message: string) {
        console.log(`Api error ${this.method} ${this.path}: ${message}`);
        if (this.onError) {this.onError(message)}
    }

    execute() {
        try {
            fetch(`${ApiClient.HOST}${this.path}`, {
                method: this.method,
                headers: {
                    "Content-Type": "application/json",
                    ...(this.user ? this.user.getAuthHeader() : {}),
                },
                body: this.data ? JSON.stringify(this.data) : null,
            }).then(
                (response) => {
                    if (!response.ok || response.status !== 200) {
                        response.json().then((data) => {this._onError(data['details'])})
                        if (response.status === 429) {alert("Too many connections due using tunnel proxy.")} // todo
                    } else {
                        response.json().then((data) => {this._onSuccess(data)})
                    }
                },
                (error) => {this._onError(error)}
            )
        } catch (error) {this._onError(error)}
    }

}


export class ApiClient {
    static HOST = `${Env.API_HOST}${Env.API_PORT ? ':'+Env.API_PORT : ''}${Env.API_PREFIX}`

    static USERS_PATH = "/users/"
    static ROOMS_PATH = "/rooms/"

    static signInSignUp(
        onSuccess: {(data: UserPrivateResponseType):void},
        onError: ErrorCallbackType,
        name: string,
        password: string,
        color: string | null = null,
    ) {
        const request = new Request(this.USERS_PATH, Method.POST)
        request.onSuccess = onSuccess
        request.onError = onError
        request.data = {"name": name, "password": password, "color": color}
        request.execute()
    }

    static getProfileInfo(onSuccess: {(data: UserPrivateResponseType):void}, onError: ErrorCallbackType, token: string) {
        const request = new Request(this.USERS_PATH+'me/')
        request.user = new User('', '', '', token)
        request.onSuccess = onSuccess
        request.onError = onError
        request.execute()
    }

    static getAllRooms(onSuccess: {(data: Array<RoomResponseType>):void}, onError: ErrorCallbackType) {
        const request = new Request(this.ROOMS_PATH)
        request.user = __getCurrentUser__()
        request.onSuccess = onSuccess
        request.onError = onError
        request.execute()
    }

    static createRoom(onSuccess: {(data: RoomResponseType):void}, onError: ErrorCallbackType, name:string) {
        const request = new Request(this.ROOMS_PATH, Method.POST)
        request.user = __getCurrentUser__()
        request.onError = onError
        request.onSuccess = onSuccess
        request.data = {"name": name}
        request.execute()
    }

    static getAllRoomsMessages(onSuccess:{(data:Array<MessageResponseType>):void}, onError: ErrorCallbackType, roomId:string) {
        const request = new Request(this.ROOMS_PATH+roomId+'/messages/')
        request.onSuccess = onSuccess
        request.onError = onError
        request.user = __getCurrentUser__()
        request.execute()
    }

    static getAllRoomsMembers(onSuccess: {(data:Array<UserResponseType>):void}, onError: ErrorCallbackType, roomId:string) {
        const request = new Request(this.ROOMS_PATH+roomId+'/members/')
        request.onSuccess = onSuccess
        request.onError = onError
        request.user = __getCurrentUser__()
        request.execute()
    }

    static getUserInfo(onSuccess: {(data:UserResponseType):void}, onError: ErrorCallbackType, lookupUserId:string) {
        const request = new Request(this.USERS_PATH+lookupUserId+'/')
        request.onSuccess = onSuccess
        request.onError = onError
        request.user = __getCurrentUser__()
        request.execute()
    }

    static getConnectedToRoomUsers(onSuccess: {(data:Array<UserResponseType>):void}, onError: ErrorCallbackType, roomId:string) {
        const request = new Request(this.ROOMS_PATH+roomId+'/watchers/')
        request.onSuccess = onSuccess
        request.onError = onError
        request.user = __getCurrentUser__()
        request.execute()
    }

    static createMessage(onSuccess: {(data: MessageResponseType):void}, onError: ErrorCallbackType, roomId: string, content: string) {
        const request = new Request(this.ROOMS_PATH + roomId + '/messages/', Method.POST)
        request.user = __getCurrentUser__()
        request.onError = onError
        request.onSuccess = onSuccess
        request.data = {"content": content}
        request.execute()
    }
}


export type UserResponseType = { id: string, name: string, color: string }
export type UserPrivateResponseType = UserResponseType & {token: string}
export type RoomResponseType = { id: string, name: string }
export type MessageResponseType = {
    id: string,
    room_id: string,
    content: string,
    user_id: string,
    user_name: string,
    order: number,
}

type ErrorCallbackType = { (error: string): void } | null
