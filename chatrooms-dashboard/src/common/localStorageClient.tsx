import {local_storage_log__} from "./loggers";


export class LocalStorageClient {
    static TOKEN_KEY = 'token'
    static getToken(): string | null {
        const value = localStorage.getItem(this.TOKEN_KEY)
        local_storage_log__(`getToken -> ${value}.`)
        return value
    }
    static setToken(token: string) {
        local_storage_log__(`setToken -> ${token}.`)
        localStorage.setItem(this.TOKEN_KEY, token)
    }
    static removeToken() {
        local_storage_log__('removeToken.')
        localStorage.removeItem(this.TOKEN_KEY)
    }
}
