import {WSEvents} from "./ws/wsEvents";
import {auth_log__, ws_log__} from "./loggers";
import {User} from "./user";
import {raiseCriticalError} from "./errors";
import {ActionFactory, initStore} from "./store/store";
import {LocalStorageClient} from "./localStorageClient";
import {defaultStylesState, StylesStateType} from "./stylesState";


/*------------------------------------------APP-WEBSOCKET-CONNECTION--------------------------------------------------*/
let _wsConnection : WSEvents | null = null;
let _lastConnectedUserId: string = '';
const _disconnectWS = () => {
    ws_log__(`trying to close last connection (${_lastConnectedUserId})`);
    _wsConnection?.ws.close();
    _lastConnectedUserId = '';
}
const _connectWS = (user: User) => {
    ws_log__(`trying to initialize new connection for ${user.name} ${user.id}`);
    if (user.id === _lastConnectedUserId) {return _wsConnection}
    if (_wsConnection) {_disconnectWS()}
    _wsConnection = new WSEvents(user);
    _lastConnectedUserId = user.id;
    return _wsConnection;
}
export const __getWSConnection__ = (): WSEvents => {
    if (!_wsConnection) {raiseCriticalError("Attempt to access not initialised WS Connection")}
    return _wsConnection!;
}
/*--------------------------------------------------------------------------------------------------------------------*/


/*--------------------------------------------APP-STORE-INITIALISATION------------------------------------------------*/
const _store = initStore();
export const __getStore__ = () => {return _store}
/*--------------------------------------------------------------------------------------------------------------------*/


/*------------------------------------------------APP-CURRENT-USER----------------------------------------------------*/
let _currentUser: User|null = null;
export const __setCurrentUser__ = (user: User) => {
    _store.dispatch(ActionFactory.addUser({id: user.id, name: user.name, color: user.color}));
    LocalStorageClient.setToken(user.token);
    _currentUser = user;
    _connectWS(user);
    auth_log__(`logged in as ${user.name}`);
}
export const __unsetCurrentUser__ = () => {
    LocalStorageClient.removeToken();
    _disconnectWS();
    auth_log__(`${_currentUser?.name} logged out`);
    _currentUser = null;
}
export const __getCurrentUser__ = (): User => {
    if (!_currentUser) {raiseCriticalError("Attempt to access not storing current user")}
    return _currentUser!;
}
/*--------------------------------------------------------------------------------------------------------------------*/


/*------------------------------------------------APP-GLOBAL-STYLES---------------------------------------------------*/
let _stylesState: StylesStateType = {...defaultStylesState};
export const __getStylesState__ = () => {return _stylesState};
/*--------------------------------------------------------------------------------------------------------------------*/
