import {Env} from "../settings";

export const WS = "WS"
export const LOCAL_STORAGE = "LOCAL_STORAGE"
export const AUTH = "AUTH"
export const USE_EFFECTS = "USE_EFFECTS"
export const STORE = "STORE"

const log = (msg: string, logger: string) => {
    if (Env.ENABLED_LOGGERS.includes(logger)) {
        const date = new Date()
        console.log(`${date.toLocaleTimeString()} | ${logger}: ${msg}`)
    }
}

export const ws_log__ = (msg: string) => {log(msg, WS)}
export const local_storage_log__ = (msg: string) => {log(msg, LOCAL_STORAGE)}
export const auth_log__ = (msg: string) => {log(msg, AUTH)}
export const use_effect_log__ = (msg: string) => {log(msg, USE_EFFECTS)}
export const store_log__ = (msg: string) => {log(msg, STORE)}
