

export class K {
    static ENTER = "Enter"
    static UP = "ArrowUp"
    static DOWN = "ArrowDown"
    static LEFT = "ArrowLeft"
    static RIGHT = "ArrowRight"
    static ESC = "Escape"
}



type HandlerFuncT = { (event: KeyboardEvent): void }
type KeyboardEvT = 'keydown'|'keyup'
type HandlerArgs = { func: HandlerFuncT, keyboardEvent?: KeyboardEvT, listenRepetitions?: boolean }

function ignoreRepeats(f: HandlerFuncT) {
    return function wrapped(event: KeyboardEvent) { if (!event.repeat) {f(event)} }
}

export const KeyboardListenersFactory = (
    handlers: HandlerArgs[] | HandlerFuncT,
    keyboardEvent: KeyboardEvT | undefined = "keydown",
    listenRepetitions: boolean = false
) => {
    // must be returned from component useEffect
    // args can be provided in array of objects to add multiple listeners
    const _handlers = Array.isArray(handlers)
        ? handlers
        : [{ func: handlers, keyboardEvent: keyboardEvent, listenRepetitions: listenRepetitions }]

    _handlers.forEach((h) => {
        if (!h?.keyboardEvent) {h.keyboardEvent = "keydown"}
        if (!h?.listenRepetitions) {h.func = ignoreRepeats(h.func)}
        document.addEventListener(h.keyboardEvent, h.func)
    })
    return () => {
        _handlers.forEach(h => {document.removeEventListener(h.keyboardEvent!, h.func)})
    }
}
