import { createStore } from "redux"
import {store_log__} from "../loggers";


export type MessageType = { id: string, content: string, user_id: string, user_name: string, room_id: string }
export type RoomType = { id: string, name: string, messages: {[key: string]: MessageType} }
export type UserType = { id: string, name: string, color: string }

export type StoreStateType = {
    rooms: {[key: string]: RoomType};
    users: {[key: string]: UserType};
    roomsConnectedUsersIds: {[key: string]: string[]}
}


type ActionT = {type: string, data: any}


class ActionType {
    static ADD_ROOM = 'ADD_ROOM'
    static ADD_MULTIPLE_ROOMS = 'ADD_ROOMS'

    static ADD_ROOM_MSG = 'ADD_MSG'
    static ADD_MULTIPLE_ROOM_MSGS = 'ADD_MSGS'

    static ADD_USER = 'ADD_USER'
    static ADD_MULTIPLE_USERS = 'ADD_USERS'

    static SET_USER_CONNECTED_TO_ROOM = 'CONNECT_USER_TO_ROOM'
    static SET_USERS_CONNECTED_TO_ROOM = 'CONNECT_USERS_TO_ROOM'  // replaces existing
    static UNSET_USER_CONNECTED_TO_ROOM = 'DISCONNECT_USER_FROM_ROOM'

}


export class ActionFactory {
    static addRoom = (room: RoomType) => {return {type: ActionType.ADD_ROOM, data: room}}
    static addMultipleRooms = (rooms: RoomType[]) => {return {type: ActionType.ADD_MULTIPLE_ROOMS, data: rooms}}
    static addRoomMsg = (msg: MessageType) => {return {type: ActionType.ADD_ROOM_MSG, data: msg}}
    static addMultipleRoomMsgs = (msgs: MessageType[]) => {return {type: ActionType.ADD_MULTIPLE_ROOM_MSGS, data: msgs}}
    static addUser = (user: UserType) => {return {type: ActionType.ADD_USER, data: user}}
    static addMultipleUsers = (users: UserType[]) => {return {type: ActionType.ADD_MULTIPLE_USERS, data: users}}
    static setUserConnectedToRoom = (userId: string, roomId: string) => {return {
        type: ActionType.SET_USER_CONNECTED_TO_ROOM, data: {userId: userId, roomId: roomId}}
    }
    static unsetUserConnectedToRoom = (userId: string, roomId: string) => {return {
        type: ActionType.UNSET_USER_CONNECTED_TO_ROOM, data: {userId: userId, roomId: roomId}
    }}
    static setMultipleUsersConnectedToRoom = (usersIds: string[], roomId: string) => {return {
        type: ActionType.SET_USERS_CONNECTED_TO_ROOM, data: {usersIds: usersIds, roomId: roomId}
    }}

}


function rootReducer(state: StoreStateType, action: ActionT) {
    store_log__("reducing " + action.type)
    switch (action.type) {

        case ActionType.ADD_ROOM: {
            if (!(action.data.id in state.rooms)) {
                return {...state, rooms: {...state.rooms, [action.data.id]: action.data}}
            }
            return state
        }


        case ActionType.ADD_MULTIPLE_ROOMS: {
            const newRooms: {[p: string]: RoomType} = {}
            action.data.forEach((room: RoomType) => {newRooms[room.id] = room})
            return newRooms ? {
                ...state,
                rooms: {
                    ...state.rooms,
                    ...newRooms,
                }
            } : state
        }

        case ActionType.ADD_ROOM_MSG: {
            const room: RoomType|undefined = state.rooms[action.data.room_id]
            if (room && !(action.data.id in room.messages)) {
                return {
                    ...state,
                    rooms: {
                        ...state.rooms,
                        [room.id]: {
                            ...room,
                            messages: {
                                ...room.messages,
                                [action.data.id]: action.data,
                            },
                        },
                    },
                }
            }
            return state
        }

        case ActionType.ADD_MULTIPLE_ROOM_MSGS: {
            const newRooms: {[p: string]: RoomType} = {}
            action.data.forEach((msg: MessageType) => {
                if (!(msg.room_id in newRooms)) { newRooms[msg.room_id] = state.rooms[msg.room_id] }
                newRooms[msg.room_id].messages = {
                    ...newRooms[msg.room_id].messages,
                    [msg.id]: msg,
                }
            })
            return newRooms ? {
                ...state,
                rooms: {
                    ...state.rooms,
                    ...newRooms,
                }
            } : state
        }

        case ActionType.ADD_USER: {
            if (!(action.data.id in state.users)) {
                return {...state, users: {...state.users, [action.data.id]: action.data}}
            }
            return state
        }

        case ActionType.ADD_MULTIPLE_USERS: {
            const newUsers: {[p: string]: UserType} = {}
            action.data.forEach((user: UserType) => {newUsers[user.id] = user})
            return newUsers ? {
                ...state,
                users: {
                    ...state.users,
                    ...newUsers,
                }
            } : state
        }

        case ActionType.SET_USER_CONNECTED_TO_ROOM: {
            if (!(action.data.roomId in state.roomsConnectedUsersIds)) {
                state.roomsConnectedUsersIds[action.data.roomId] = []
            } else if (state.roomsConnectedUsersIds[action.data.roomId].includes(action.data.userId)) {
                return state
            }

            return {
                ...state,
                roomsConnectedUsersIds: {
                    ...state.roomsConnectedUsersIds,
                    [action.data.roomId]: [...state.roomsConnectedUsersIds[action.data.roomId], action.data.userId]
                }
            }
        }

        case ActionType.SET_USERS_CONNECTED_TO_ROOM: {
            return {
                ...state,
                roomsConnectedUsersIds: {
                    ...state.roomsConnectedUsersIds,
                    [action.data.roomId]: action.data.usersIds
                }
            }
        }

        case ActionType.UNSET_USER_CONNECTED_TO_ROOM: {
            if (!(action.data.roomId in state.roomsConnectedUsersIds)) {
                return state
            }
            const userIdIndex = state.roomsConnectedUsersIds[action.data.roomId].findIndex((v) => v === action.data.userId)
            return {
                ...state,
                roomsConnectedUsersIds: {
                    ...state.roomsConnectedUsersIds,
                    [action.data.roomId]: [
                        ...state.roomsConnectedUsersIds[action.data.roomId].slice(0, userIdIndex),
                        ...state.roomsConnectedUsersIds[action.data.roomId].slice(userIdIndex + 1)
                    ]
                }
            }
        }

        default:
            return state
    }
}


export function initStore() {
    return createStore(
        // @ts-ignore
        rootReducer,
        {rooms: {}, users: {}, roomsConnectedUsersIds: {}},
        (window as any).__REDUX_DEVTOOLS_EXTENSION__ && (window as any).__REDUX_DEVTOOLS_EXTENSION__(),
    )
}
