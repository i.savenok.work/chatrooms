

export function raiseCriticalError(message: string) {
    throw new Error(message)
}
