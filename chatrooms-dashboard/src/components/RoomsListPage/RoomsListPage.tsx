import React, {useEffect, useState} from "react";
import {ApiClient} from "../../common/apiClient";
import {use_effect_log__} from "../../common/loggers";
import {ActionFactory, RoomType} from "../../common/store/store";
import {useDispatch} from "react-redux";
import {__getWSConnection__} from "../../common/globals";
import {roomResponseToRoom} from "../../common/transformators";
import {RoomsListHuge} from "./RoomsListHuge/RoomsListHuge";
import {RoomsListSmall} from "./RoomsListSmall/RoomsListSmall";
import {Room} from "./Room/Room";


export function RoomsListPage(props: {}) {
    const dispatch = useDispatch()
    const [currentRoom, _setCurrentRoom] = useState<RoomType|null>(null);

    const changeCurrentRoom = (room: RoomType | null) => {
        if (currentRoom) {__getWSConnection__().sendUserLeftRoom()}
        if (room) {__getWSConnection__().sendUserEnteredRoom({room_id: room.id})}
        _setCurrentRoom(room);
    }

    useEffect(() => {
        use_effect_log__('RoomsListPage, []')
        ApiClient.getAllRooms(
            (roomsData) => {
                dispatch(ActionFactory.addMultipleRooms(roomsData.map(roomResponseToRoom)))
            },
            null,
        )
    }, [dispatch])

    if (currentRoom) {
        return (
            <div>
                <RoomsListSmall onRoomSelected={changeCurrentRoom}/>
                <Room room={currentRoom} onClose={() => {changeCurrentRoom(null)}} />
            </div>
        )
    } else {
        return <RoomsListHuge onRoomSelected={changeCurrentRoom} />
    }
}
