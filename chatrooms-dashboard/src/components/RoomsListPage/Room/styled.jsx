import styled from "styled-components";
import {__getStylesState__} from "../../../common/globals";
import {ReactComponent as BackArrowIcon} from "../../../assets/svg/back-arrow.svg";
import {ReactComponent as SendMsgIcon} from "../../../assets/svg/send.svg";
import {LoadingWidget} from "../../common/LoadingWidget/LoadingWidget";
import React, {useEffect, useState} from "react";


export const RoomContentTileContainer = styled.div`
    position: fixed;
    top: 0;
    right: 0;
    width: 75%;
    min-width: 700px;

`


export const RoomTitleContainer = styled.div`
    background-color: ${__getStylesState__().commonSecondBackgroundColor};

    position: fixed;
    top: 0;
    width: 900px;
    min-width: 700px;
    height: 40px;
    z-index: 10;

    border-bottom-left-radius: 20px;
    border-bottom-right-radius: 20px;

    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 24px;
`;
const BackArrowIconWrapper = styled.div`
    position: absolute;

    top: 5px;
    right: 5px;

    width: 30px;
    height: 30px;
    border-radius: 20px;

    display: flex;
    align-items: center;
    justify-content: center;

    &:hover {
        background-color: ${__getStylesState__().commonThirdBackgroundColor};
    }
`
export const BackToRoomsListButton = (props) => {
    return (<BackArrowIconWrapper onClick={props.onClick}>
        <BackArrowIcon style={{height: "20px", width: "20px", color: __getStylesState__().commonFirstColor}}/>
    </BackArrowIconWrapper>)
}

export const RMWrapper = styled.div`
    position:fixed;
    width: 900px;
    min-width: 700px;
    bottom: 8vh;
    height: 87%;
    border-radius: 20px;
    overflow: hidden;
`;
export const RMScroll = styled.div`
    background-color: ${__getStylesState__().commonSecondBackgroundColor};

    height: 100%;
    overflow-y:auto;
    overflow-x:hidden;


    ::-webkit-scrollbar {
        background-color: red;
    }

    ::-webkit-scrollbar-thumb {

    }
`;
export const RMContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: flex-end;
    min-height: 98.8%;
    margin: 5px 10px 5px 10px;
`;

export const CreateMessageFormWrapper = styled.div`
    position: fixed;
    bottom: 3vh;
    z-index: 9;
    width: 900px;
    min-width: 700px;
    border-radius: 20px;
    height: 40px;

`;
export const CreateMessageTextArea = styled.textarea`
    background-color: ${__getStylesState__().commonSecondBackgroundColor};
    color: ${__getStylesState__().commonFirstColor};
    height: 100%;
    width: 97%;
    min-width: 700px;
    border-radius: 20px;
    border: none;
    font-size: 14px;
    padding-left: 1.5%;
    padding-right: 1.5%;
`;
const SendMessageButtonWrapper = styled.div`
    position: absolute;
    right: 10px;
    top: 7px;
    opacity: 0.8;
    z-index: 1;
`;
const SendMessageButtonLoadingWidgetWrapper = styled.div`
    transform: translateY(-20px);
`;
export const SendMessageButton = (props) => {
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        if (props.isLoading) {
            setIsLoading(true)
        }
    }, [props.isLoading])

    return (<SendMessageButtonWrapper onClick={props.onClick}>
        <SendMsgIcon style={{
            height: "15px",
            width: "15px",
            fill: __getStylesState__().commonFirstColor,
            visibility: isLoading ? "hidden" : "visible",
        }}/>
        <SendMessageButtonLoadingWidgetWrapper>
            <LoadingWidget
                active={props.isLoading}
                color={__getStylesState__().commonFirstColor}
                size="15px"
                onAnimationEnd={() => setIsLoading(false)}
            />
        </SendMessageButtonLoadingWidgetWrapper>
    </SendMessageButtonWrapper>)
}
export const CreateMessageFormErrorMsg = styled.div`
    color: #e02d2d;
    font-size: 12px;
    position: absolute;
    top: 120%;
    padding-left: 20px;
    padding-right: 20px;
`;

export const RoomConnectedUsersWidgetWrapper = styled.div`
    position: fixed;
    top: 48px;
    left: 1368px;
`;

const RMMessageWrapper = styled.div`
    background-color: ${props =>
        props.color
            ? props.color+'15'  // opacity
            : __getStylesState__().commonSecondBackgroundColor
    };

    margin-top: 5px;
    border-radius: 25px;
    padding-left: 20px;
    padding-right: 20px;
`;
const RMMessageAuthorContainer = styled.div`
    color: ${props => props.color ? props.color : __getStylesState__().commonFirstColor};
    font-size: 12px;
    font-weight: bold;
    padding-top: 2px;
`;
const RMMessageContentContainer = styled.div`
    padding-bottom: 2px;
    //background-color: yellow;
    overflow-wrap: break-word;
`;
export const RMMessageContainer = (props) => {
    return (<RMMessageWrapper color={props?.user?.color}>
        <RMMessageAuthorContainer color={props?.user?.color}>{props?.user?.name}</RMMessageAuthorContainer>
        <RMMessageContentContainer>{props.message.content}</RMMessageContentContainer>
    </RMMessageWrapper>)
}
