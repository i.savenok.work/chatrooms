import {ActionFactory, RoomType, StoreStateType} from "../../../common/store/store";
import {useDispatch, useSelector} from "react-redux";
import React, {useEffect, useRef, useState} from "react";
import {use_effect_log__} from "../../../common/loggers";
import {ApiClient, MessageResponseType} from "../../../common/apiClient";
import {messageResponseToMessage, userResponseToUser} from "../../../common/transformators";
import {RoomConnectedUsersWidget} from "../RoomConnectedUsersWidget/RoomConnectedUsersWidget";
import {
    BackToRoomsListButton, CreateMessageFormErrorMsg,
    CreateMessageFormWrapper, CreateMessageTextArea, RMContainer, RMMessageContainer, RMScroll, RMWrapper,
    RoomConnectedUsersWidgetWrapper,
    RoomContentTileContainer,
    RoomTitleContainer, SendMessageButton
} from "./styled";
import {K, KeyboardListenersFactory} from "../../../common/keyboard";


export function Room(props: { room: RoomType, onClose: {():void} }) {
    const dispatch = useDispatch()
    const messages = useSelector((state: StoreStateType) => state.rooms[props.room.id].messages)
    const users = useSelector((state: StoreStateType) => state.users)
    const messagesScrollbarRef = useRef<HTMLDivElement>();

    const [scrollbarScrolledFirstTime, setScrollbarScrolledFirstTime] = useState(false);

    useEffect(() => {
        use_effect_log__('Room, [dispatch]')
        ApiClient.getAllRoomsMessages(
            (messages) => {
                dispatch(ActionFactory.addMultipleRoomMsgs(messages.map(messageResponseToMessage)))
            },
            null,
            props.room.id,
        )
        ApiClient.getAllRoomsMembers(
            (members) => {
                dispatch(ActionFactory.addMultipleUsers(members.map(userResponseToUser)))
            },
            null,
            props.room.id,
        )
        ApiClient.getConnectedToRoomUsers(
            (users) => {
                dispatch(ActionFactory.addMultipleUsers(users))
                dispatch(ActionFactory.setMultipleUsersConnectedToRoom(users.map(u => u.id), props.room.id))
            },
            null,
            props.room.id,
        )
    }, [dispatch, props.room])

    useEffect(() => {
        use_effect_log__('Room, []')
        if (messagesScrollbarRef.current  && Object.keys(messages).length !== 0) {

            if (!scrollbarScrolledFirstTime) {
                scrollScrollbarToTheBottom(); setScrollbarScrolledFirstTime(true);}
            /*
                element.scrollTop - is the pixels hidden in top due to the scroll. With no scroll its value is 0.
                element.scrollHeight - is the pixels of the whole div.
                element.clientHeight - is the pixels that you see in your browser.
            */
            const scrollBar = messagesScrollbarRef.current
            const viewInScrollBarBottomPosition = (
                scrollBar?.scrollHeight -
                scrollBar?.clientHeight -
                scrollBar?.scrollTop
            )
            if (viewInScrollBarBottomPosition < 120) {scrollScrollbarToTheBottom()}
        }
    }, [messages])

    const scrollScrollbarToTheBottom = () => {
        messagesScrollbarRef.current?.scrollTo(
            { behavior: 'smooth', top: messagesScrollbarRef.current?.scrollHeight }
        )
    }

    const handleKeyEvent = (event: KeyboardEvent) => {if (event.key === K.ESC) {props.onClose()}}

    useEffect(() => {
        use_effect_log__('Room, []')
        return KeyboardListenersFactory(handleKeyEvent)
    }, [])

    return (
        <RoomContentTileContainer>
            <RoomTitleContainer>
                {props.room.name}
                <BackToRoomsListButton onClick={props.onClose}/>
            </RoomTitleContainer>
            <RMWrapper>
            <RMScroll ref={messagesScrollbarRef}>
                <RMContainer>
                    {Object.values(messages).map((msg) => {
                        return <RMMessageContainer user={users[msg.user_id]} message={msg} key={msg.id}/>
                    })}
                </RMContainer>
            </RMScroll>
            </RMWrapper>
            <CreateMessageForm
                room={props.room}
                onCreated={
                    (msgData) => dispatch(ActionFactory.addRoomMsg(messageResponseToMessage(msgData)))
                }
            />
            <RoomConnectedUsersWidgetWrapper>
                <RoomConnectedUsersWidget roomId={props.room.id} />
            </RoomConnectedUsersWidgetWrapper>
        </RoomContentTileContainer>
    )
}


function CreateMessageForm(props: { room: RoomType, onCreated: {(data: MessageResponseType):void} }) {
    const [content, setContent] = useState('');

    const [requestInProgress, setRequestInProgress] = useState(false);
    const [errorMsg, setErrorMsg] = useState('');

    useEffect(() => {
        use_effect_log__('CreateMessageForm, [requestInProgress]')
        if (requestInProgress) {
            setErrorMsg('')
            ApiClient.createMessage(
                (data) => {
                    console.log(content)
                    setContent('');
                    setRequestInProgress(false);
                    props.onCreated(data)
                },
                (errorMsg) => {
                    setErrorMsg(errorMsg);
                    setRequestInProgress(false);
                },
                props.room.id,
                content,
            )
        }
    }, [requestInProgress])

    return (
        <CreateMessageFormWrapper>
            <SendMessageButton onClick={() => {setRequestInProgress(true)}} isLoading={requestInProgress}/>
            <CreateMessageTextArea
                onChange={(e: any) => {setContent(e.target.value)}}
                value={content}
                onKeyDown={(event: any) => {
                    if (event.key === "Enter" && !(event.shiftKey)) {setRequestInProgress(true)}
                }}
                autoFocus
            />
            <CreateMessageFormErrorMsg >{errorMsg}</CreateMessageFormErrorMsg>
        </CreateMessageFormWrapper>

    )
}
