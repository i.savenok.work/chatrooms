import {ActionFactory, RoomType, StoreStateType} from "../../../common/store/store";
import {useDispatch, useSelector} from "react-redux";
import {ApiClient, RoomResponseType} from "../../../common/apiClient";
import {roomResponseToRoom} from "../../../common/transformators";
import React, {useEffect, useRef, useState} from "react";
import {use_effect_log__} from "../../../common/loggers";
import {
    CreateNewRoomButton,
    CreateRoomFormContainer,
    CreateRoomFormWrapper, CreatingRoomNameInput,
    RoomsListHugeContainer,
    RoomsListHugeRoomContainer
} from "./styled";
import {SignInSignUoFormErrorMsg} from "../../SignInSignUpPage/styled";
import {K, KeyboardListenersFactory} from "../../../common/keyboard";


export function RoomsListHuge(props: { onRoomSelected: {(room: RoomType):void} }) {
    const dispatch = useDispatch()
    const rooms = useSelector(((state: StoreStateType) => state.rooms))

    const CREATE_ROOM_FORM_CODE = 'create_room_form'
    const [focusedItemKey, setFocusedItemKey] = useState(CREATE_ROOM_FORM_CODE);

    const handleKeyEvent = (event: KeyboardEvent) => {
        if (event.repeat) {
            return
        } else if (event.key === K.ENTER) {
            if (focusedItemKey != CREATE_ROOM_FORM_CODE) {
                props.onRoomSelected(rooms[focusedItemKey])
            }
        } else if ([K.UP, K.DOWN].includes(event.key)) {
            let nextItemToFocus: string|undefined;
            const allItemsKeys = [...Object.values(rooms).map(r => r.id), CREATE_ROOM_FORM_CODE]

            const increment: number = {[K.UP]: +1, [K.DOWN]: -1,}[event.key]!
            if (focusedItemKey) {
                const currentFocusedItemPosition = allItemsKeys.findIndex(k => k === focusedItemKey)
                nextItemToFocus = allItemsKeys[currentFocusedItemPosition + increment]
            } else {
                nextItemToFocus = allItemsKeys[
                    ((event.key === K.UP) ? -1 : allItemsKeys.length) + increment
                    ]
            }

            if (nextItemToFocus!==undefined) {setFocusedItemKey(nextItemToFocus)}
        }
    }

    useEffect(() => {
        use_effect_log__('RoomsListHuge, [rooms, focusedItemKey]')
        return KeyboardListenersFactory(handleKeyEvent)
    }, [rooms, focusedItemKey])

    return (
        <RoomsListHugeContainer>
            {Object.values(rooms).map((room, i) => {
                return <RoomsListHugeRoomContainer
                        key={room.id}
                        roomName={room.name}
                        onClick={(event: any) => props.onRoomSelected(room)}

                        focused={focusedItemKey == room.id}
                        onMouseEnter={(ev: any) => {setFocusedItemKey(room.id)}}
                        onMouseLeave={(ev: any) => {setFocusedItemKey('')}}
                        onMouseMove={(ev: any) => {setFocusedItemKey(room.id)}}
                />
            })}

            <CreateRoomFormContainer>
                <CreateRoomForm
                    onCreated={(roomData: RoomResponseType) => {
                        const createdRoom = roomResponseToRoom(roomData);
                        dispatch(ActionFactory.addRoom(createdRoom));
                        // props.onRoomSelected(createdRoom); todo
                    }}
                    focus={focusedItemKey === CREATE_ROOM_FORM_CODE}
                />
            </CreateRoomFormContainer>


        </RoomsListHugeContainer>
    )
}


function CreateRoomForm(props: { onCreated: {(data: RoomResponseType):void}, focus: boolean }) {
    const [name, setName] = useState('');

    const [requestInProgress, setRequestInProgress] = useState(false);
    const [errorMsg, setErrorMsg] = useState('');

    const nameInputRef = useRef<HTMLInputElement>();

    useEffect(() => {
        use_effect_log__('CreateRoomForm, [requestInProgress]')
        if (requestInProgress) {
            ApiClient.createRoom(
                (data) => {
                    setErrorMsg('');
                    setName('');
                    setRequestInProgress(false);
                    props.onCreated(data);
                },
                (errorMsg) => {
                    setErrorMsg(errorMsg.toString());
                    setRequestInProgress(false);
                    nameInputRef.current?.focus()
                },
                name,
            )
        }
    }, [requestInProgress])

    useEffect(() => {
        use_effect_log__('CreateRoomForm, [props.focus]')
        if (props.focus) {nameInputRef.current?.focus()} else {nameInputRef.current?.blur()}
    }, [props.focus])

    return (
            <CreateRoomFormWrapper>
                <CreateNewRoomButton
                    onClick={() => {setRequestInProgress(true)}}
                    isLoading = {requestInProgress}
                />
                <CreatingRoomNameInput
                    className="input-1"
                    onChange={(e: any) => {setName(e.target.value)}}
                    value={name}
                    placeholder="New room"
                    ref={nameInputRef}
                    onKeyDown={(event: any) => {
                        if (props.focus && event.key === "Enter") {setRequestInProgress(true)}
                    }}
                />
                <SignInSignUoFormErrorMsg >{errorMsg}</SignInSignUoFormErrorMsg>
            </CreateRoomFormWrapper>
    )
}
