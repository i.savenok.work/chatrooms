import styled from "styled-components";
import {__getStylesState__} from "../../../common/globals";
import {ReactComponent as PlusIcon} from "../../../assets/svg/plus.svg";
import {ReactComponent as HouseIcon} from "../../../assets/svg/house.svg";
import {LoadingWidget} from "../../common/LoadingWidget/LoadingWidget";
import {useEffect, useState} from "react";


export const RoomsListHugeContainer = styled.div`
    background-color: ${__getStylesState__().commonSecondBackgroundColor};
    width: 50%;
    height: auto;
    min-height: 100vh;
    margin-left: 25%;

    display: flex;
    flex-direction: column-reverse;
    justify-content: start;
`;


export const CreateRoomFormContainer = styled.div`
    background-color: ${__getStylesState__().commonThirdBackgroundColor};
    margin: 5px 5px 0 5px;
    height: 35px;
    border-radius: 20px;
`;


export const RoomsListHugeRoomContainerWrapper = styled.div`
    margin: 5px 5px 0 5px;
    height: 35px;
    border-radius: 20px;

    display: flex;
    align-items: center;

    background-color: ${props =>
        props.focused ? __getStylesState__().commonHoverBackgroundColor : __getStylesState__().commonThirdBackgroundColor
    }; // props.focused is instead of hover

    &:active {
        background-color: ${__getStylesState__().commonActivatedBackgroundColor};
    }
`;
export function RoomsListHugeRoomContainer(props) {
    return (
        <RoomsListHugeRoomContainerWrapper
            onClick={props.onClick}
            focused={props.focused}
            onMouseEnter={props.onMouseEnter}
            onMouseLeave={props.onMouseLeave}
            onMouseMove={props.onMouseMove}
        >
            <HouseIcon style={{
                height: "20px",
                width: "20px",
                fill: __getStylesState__().commonSecondBackgroundColor,
                margin: "0 15px 0 7px",
            }}
            />{props.roomName}
        </RoomsListHugeRoomContainerWrapper>
    )
}


export const CreateRoomFormWrapper = styled.div`
    display: flex;
    align-items: center;
`;
const CreateNewRoomButtonWrapper = styled.div`
    height: 35px;
    width: 35px;
    border-radius: 20px;

    display: flex;
    align-items: center;
    justify-content: center;

    &:hover {
        background-color: ${__getStylesState__().commonHoverBackgroundColor};
    }

    &:active {
        background-color: ${__getStylesState__().commonActivatedBackgroundColor};
    }
`;
const CreateRoomButtonLoadingWidgetWrapper = styled.div`
    position: absolute;
    top: 12px;
`;
export function CreateNewRoomButton(props) {
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        if (props.isLoading) {setIsLoading(true)}
    }, [props.isLoading])

    return (
        <CreateNewRoomButtonWrapper onClick={props.onClick}>
            <PlusIcon style={{
                height: "20px",
                width: "20px",
                fill: __getStylesState__().commonSecondBackgroundColor,
                visibility: isLoading ? "hidden" : "visible"
            }}/>
            <CreateRoomButtonLoadingWidgetWrapper>
                <LoadingWidget active={props.isLoading} onAnimationEnd={() => setIsLoading(false)}/>
            </CreateRoomButtonLoadingWidgetWrapper>
        </CreateNewRoomButtonWrapper>)
}
export const CreatingRoomNameInput = styled.input`
    background-color: ${__getStylesState__().commonThirdBackgroundColor};
    margin-left: 5px;
    border: 0;
    border-bottom: solid ${__getStylesState__().commonFirstColor} 1px;
    width: auto;
    color: ${__getStylesState__().commonFirstColor};

    &::placeholder {
        opacity: 1;
        color: ${__getStylesState__().commonSecondColor};
    }

    &:focus {
        border-color: ${__getStylesState__().commonActivatedBackgroundColor};
    }
`;
