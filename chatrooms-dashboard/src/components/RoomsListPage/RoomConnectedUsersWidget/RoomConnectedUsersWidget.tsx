import {useSelector} from "react-redux";
import {StoreStateType, UserType} from "../../../common/store/store";
import React from "react";
import {UsersWidgetUserContainer, UsersWidgetWrapper} from "./styled";


export function RoomConnectedUsersWidget(props: {roomId: string}) {
    const connectedUsersIds = useSelector((state: StoreStateType) => state.roomsConnectedUsersIds[props.roomId])
    const users = useSelector((state: StoreStateType) => state.users)

    return (
        <UsersWidgetWrapper>
            {connectedUsersIds?.map(userId => {
                const user: UserType|undefined = users[userId]
                return <UsersWidgetUserContainer
                    key={userId}
                    userName={user ? user.name : "..."}
                    userColor={user?.color}
                />
            })}
        </UsersWidgetWrapper>
    )
}
