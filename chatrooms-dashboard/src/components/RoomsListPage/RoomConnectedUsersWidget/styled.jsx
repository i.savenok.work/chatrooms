import styled from "styled-components";
import {ReactComponent as ManInTieIcon} from "../../../assets/svg/man-in-tie.svg";
import {__getStylesState__} from "../../../common/globals";


export const UsersWidgetWrapper = styled.div`
    background-color: ${__getStylesState__().commonSecondBackgroundColor};
    padding: 5px;
    border-radius: 20px;

    display: flex;
    flex-direction: column-reverse;
`;

const UsersWidgetUserContainerWrapper = styled.div`
    display: flex;
    align-content: center;
    margin-bottom: 3px;
    color: ${props => props.color ? props.color : __getStylesState__().commonFirstColor}
`;

export function UsersWidgetUserContainer(props) {
    return (
        <UsersWidgetUserContainerWrapper color={props.userColor}>
            <ManInTieIcon style={{
                height: "18px",
                width: "18px",
                fill: props.userColor ? props.userColor : __getStylesState__().commonSecondBackgroundColor,
                marginRight: "5px",
            }}
            />{props.userName ? props.userName : 'UNKNOWN'}
        </UsersWidgetUserContainerWrapper>
    )
}
