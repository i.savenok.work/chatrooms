import {RoomType, StoreStateType} from "../../../common/store/store";
import {useSelector} from "react-redux";
import React from "react";
import {
    RoomsListSmallContainer,
    RoomsListSmallRoomContainer,
    RoomsListSmallRoomsListContainer, RoomsListSmallRoomsListScroll, RoomsListSmallRoomsListScrollWrapper,
    RoomsListSmallTitleContainer
} from "./styled";


export function RoomsListSmall(props: { onRoomSelected: {(room: RoomType):void} }) {
    const rooms = useSelector(((state: StoreStateType) => state.rooms))
    return (
        <RoomsListSmallContainer>
            <RoomsListSmallTitleContainer>
                Other rooms
            </RoomsListSmallTitleContainer>
            <RoomsListSmallRoomsListScrollWrapper>
                <RoomsListSmallRoomsListScroll>
                    <RoomsListSmallRoomsListContainer>
                        {Object.values(rooms).map((room) => {
                            return <RoomsListSmallRoomContainer
                                key={room.id}
                                onClick={(e: any) => {props.onRoomSelected(room)}}
                                roomName={room.name}
                            />
                        })}
                    </RoomsListSmallRoomsListContainer>
                </RoomsListSmallRoomsListScroll>
            </RoomsListSmallRoomsListScrollWrapper>
        </RoomsListSmallContainer>
    )
}
