import styled from "styled-components";
import {ReactComponent as HouseIcon} from "../../../assets/svg/house.svg";
import {__getStylesState__} from "../../../common/globals";


export const RoomsListSmallContainer = styled.div`
    width: 430px;
    height: 450px;

    top: 0;
    margin-left: 25px;
`;

export const RoomsListSmallTitleContainer = styled.div`
    background-color: ${__getStylesState__().commonSecondBackgroundColor};

    height: 40px;

    border-bottom-left-radius: 20px;
    border-bottom-right-radius: 20px;

    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 18px;
`;
export const RoomsListSmallRoomsListScrollWrapper = styled.div`
    overflow: hidden;
    border-radius: 20px;
    //height: 500px;
    margin-top: 6px;

`;
export const RoomsListSmallRoomsListScroll = styled.div`
    padding-bottom: 5px;
    background-color: ${__getStylesState__().commonSecondBackgroundColor};
    overflow-y:auto;
    overflow-x:hidden;
    max-height: 495px;
`;
export const RoomsListSmallRoomsListContainer = styled.div`
    display: flex;
    flex-direction: column-reverse;
`;

const RoomsListSmallRoomContainerWrapper = styled.div`
    background-color: ${__getStylesState__().commonThirdBackgroundColor};
    margin: 5px 5px 0 5px;
    height: 25px;
    border-radius: 20px;

    display: flex;
    align-items: center;

    font-size: 14px;

    &:hover {
        background-color: ${__getStylesState__().commonHoverBackgroundColor};
    }

    &:active {
        background-color: ${__getStylesState__().commonActivatedBackgroundColor};
    }
`;


export function RoomsListSmallRoomContainer(props) {
    return (
        <RoomsListSmallRoomContainerWrapper onClick={props.onClick}>
            <HouseIcon style={{
                height: "20px",
                width: "20px",
                fill: __getStylesState__().commonSecondBackgroundColor,
                margin: "0 15px 0 7px",
            }}
            />{props.roomName}
        </RoomsListSmallRoomContainerWrapper>
    )
}
