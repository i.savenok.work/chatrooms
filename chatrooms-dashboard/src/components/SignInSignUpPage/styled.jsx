import styled from "styled-components";
import {__getStylesState__} from "../../common/globals";


export const SignInSignUpTileWrapper = styled.div`
    width: 600px;
    height: 100vh;

    position: relative;

    margin: 0 auto;
`


export const SignInSignUpFormContainer = styled.div`
    width: 100%;

    position: absolute;
    top: 30%;

    display: flex;
    flex-wrap: wrap;
`

export const SignUpSignInButtonContainer = styled.div`
    background-color: ${__getStylesState__().commonSecondBackgroundColor};

    width: fit-content;
    padding: 2% 2% 2% 2%;

    border-radius: 15px 15px 0 0;
`
export const SignInSignUpButton = styled.button`
    background-color: ${__getStylesState__().commonThirdBackgroundColor};
    color: ${__getStylesState__().commonSecondBackgroundColor};
    border: 0;
    padding: 2%;
    width: 150px;
    border-radius: 7px;

    &:hover {
        background-color: ${__getStylesState__().commonHoverBackgroundColor};
    }

    &:active {
        background-color: ${__getStylesState__().commonActivatedBackgroundColor};
    }
`

export const SignUpSignInFormNotOpaqueTile = styled.div`
    background-color: ${__getStylesState__().commonSecondBackgroundColor};
    width: 20px;
    height: 20px;
    align-self: flex-end;
    z-index: 1;
`
export const SignUpSignInFormOpaqueTile = styled.div`
    background-color: ${__getStylesState__().commonFirstBackgroundColor};
    width: 100px;
    transform: translateX(-20px);
    border-radius: 15px;
    z-index: 2;
`
export const LoadingWidgetInSignInSignUpFormWrapper = styled.div`
    padding-top: 2%;
    transform: translateX(-520%);
    z-index: 3;
`


export const SignInSignUpFormInputsContainer = styled.div`
    background-color: ${__getStylesState__().commonSecondBackgroundColor};
    width: 100%;
    padding: 3% 2% 2% 2%;
    border-radius: 0 15px 15px 15px;

    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
`


export const UserNameInput = styled.input`
    background-color: ${__getStylesState__().commonSecondBackgroundColor};
    width: 275px;
    border: none;
    border-bottom: solid ${__getStylesState__().commonThirdBackgroundColor} 2px;
    color: ${props => props.color ? props.color : __getStylesState__().commonFirstColor};
    font-size: 16px;

    &::placeholder {
        opacity: 1;
        color: ${__getStylesState__().commonSecondColor};
    }

    &:focus {
        border-color: ${__getStylesState__().commonActivatedBackgroundColor};
    }
`
export const UserPasswordInput = UserNameInput
export const ColorPickerWrapper = styled.div`
    margin-top: 4%;
    width: 100%;
    color: ${__getStylesState__().commonSecondColor};
`

export const SignInSignUoFormErrorMsg = styled.div`
    color: #e02d2d;
    font-size: 14px;
    padding: 1% 2%
;
`
