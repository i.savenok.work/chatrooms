import React, {ChangeEvent, useEffect, useRef, useState} from 'react';
import {ApiClient, UserPrivateResponseType} from "../../common/apiClient";
import {User} from "../../common/user";
import {use_effect_log__} from "../../common/loggers";
import {LoadingWidget} from "../common/LoadingWidget/LoadingWidget";
import {userResponseToUserObject} from "../../common/transformators";
import { SliderPicker } from "react-color"
import {
    ColorPickerWrapper, LoadingWidgetInSignInSignUpFormWrapper, SignInSignUoFormErrorMsg,
    SignInSignUpButton,
    SignInSignUpFormContainer, SignInSignUpFormInputsContainer,
    SignInSignUpTileWrapper, SignUpSignInButtonContainer, SignUpSignInFormNotOpaqueTile, SignUpSignInFormOpaqueTile,
    UserNameInput,
    UserPasswordInput
} from "./styled";
import {K, KeyboardListenersFactory} from "../../common/keyboard";


export function SignInSignUpPage(props: { userSetter: {(user: User):void} }) {
    return (
        <SignInSignUpTileWrapper>
            <SignInSignUpForm userDataHandler={(userData: UserPrivateResponseType) => {
                props.userSetter(userResponseToUserObject(userData))
            }}/>
        </SignInSignUpTileWrapper>
    )
}


function SignInSignUpForm(props: { userDataHandler: {(userData: UserPrivateResponseType):void} }) {
    const [name, setName] = useState('');
    const [password, setPassword] = useState('');
    const [color, setColor] = useState('#c40e69');

    const [requestInProgress, setRequestInProgress] = useState(false);
    const [errorMsg, setErrorMsg] = useState('');

    const nameInputRef = useRef<HTMLInputElement>();
    const passwordInputRef = useRef<HTMLInputElement>();

    useEffect(
        () => {
            use_effect_log__('SignInSignUpForm, [requestInProgress]')
            if (requestInProgress) {
                setErrorMsg('')
                ApiClient.signInSignUp(
                    (userData) => {
                        setRequestInProgress(false);
                        props.userDataHandler(userData);
                    },
                    (error) => {
                        setErrorMsg(error.toString())
                        setRequestInProgress(false)
                        nameInputRef.current?.focus()
                    },
                    name, password, color,
                )
            }
       }, [requestInProgress]
    )

    const handleKeyEvent = (e: KeyboardEvent) => {
        switch (e.key) {
            case K.ENTER: setRequestInProgress(true); break;
            case K.LEFT: nameInputRef.current?.focus(); break;
            case K.RIGHT: passwordInputRef.current?.focus(); break;
        }
    }

    useEffect(() => {
        use_effect_log__('SignInSignUpForm, []')
        nameInputRef.current?.focus()  // focus on name input by default
        return KeyboardListenersFactory(handleKeyEvent)
    }, [])

    return (
        <SignInSignUpFormContainer>

            <SignUpSignInButtonContainer>
                <SignInSignUpButton
                    onClick={() => {setRequestInProgress(true)}}
                >
                    Sign In | Sign Up
                </SignInSignUpButton>
            </SignUpSignInButtonContainer>

            <SignUpSignInFormNotOpaqueTile/><SignUpSignInFormOpaqueTile/>
            <LoadingWidgetInSignInSignUpFormWrapper>
                <LoadingWidget active={requestInProgress}/>
            </LoadingWidgetInSignInSignUpFormWrapper>

            <SignInSignUpFormInputsContainer>
                <UserNameInput
                    color = {color}
                    onChange={(e: ChangeEvent<HTMLInputElement>) => {setName(e.target.value)}}
                    value={name}
                    placeholder={"Name"}
                    ref={nameInputRef}
                />
                <UserPasswordInput
                    onChange={(e: ChangeEvent<HTMLInputElement>) => {setPassword(e.target.value)}}
                    value={password}
                    placeholder={"Password"}
                    type="password"
                    ref={passwordInputRef}
                />
                <ColorPickerWrapper>
                    Color:
                    <SliderPicker
                        color={color}
                        onChange={(c, e) => {setColor(c.hex)}}
                    />
                </ColorPickerWrapper>
            </SignInSignUpFormInputsContainer>

            <SignInSignUoFormErrorMsg>{errorMsg}</SignInSignUoFormErrorMsg>
        </SignInSignUpFormContainer>
    )
}
