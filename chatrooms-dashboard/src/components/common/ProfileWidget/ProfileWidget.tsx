import {__getCurrentUser__} from "../../../common/globals";
import React from "react";
import {LogoutButton, ProfileWidgetWrapper} from "./styled";


export function ProfileWidget(props: { onLogOutClicked: {(): void} }) {
    const user = __getCurrentUser__()
    return (<ProfileWidgetWrapper color={user.color}>
        {user.name}<LogoutButton onClick={props.onLogOutClicked}>Logout</LogoutButton>
    </ProfileWidgetWrapper>)
}
