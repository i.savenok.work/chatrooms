import styled from "styled-components";
import {__getStylesState__} from "../../../common/globals";
import {ReactComponent as ExitIcon} from "../../../assets/svg/exit.svg";


export const ProfileWidgetWrapper = styled.div`
    background-color: ${__getStylesState__().commonSecondBackgroundColor};
    position: fixed;
    z-index: 1000;
    width: fit-content;
    padding: 7px 40px 0 10px;
    height: 35px;
    right: 0;
    border-bottom-left-radius: 20px;
    font-size: 18px;
    color: ${props => props.color ? props.color : __getStylesState__().commonFirstColor};
    border: solid ${__getStylesState__().commonFirstBackgroundColor} 1px;
`;


const LogoutButtonWrapper = styled.div`
    width: 30px;
    height: 30px;
    border-radius: 20px;

    position: absolute;
    top: 5px;
    right: 5px;

    display: flex;
    align-items: center;
    justify-content: center;

    color: ${__getStylesState__().commonSecondBackgroundColor};

    &:hover {
        background-color: ${__getStylesState__().commonThirdBackgroundColor};
    }
`


export function LogoutButton(props) {
    return (<LogoutButtonWrapper onClick={props.onClick}>
        <ExitIcon style={{height: "20px", width: "20px", fill: __getStylesState__().commonFirstColor}}/>
    </LogoutButtonWrapper>)
}
