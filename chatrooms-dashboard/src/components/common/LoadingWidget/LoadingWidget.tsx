import {ReactComponent as ArrowsIcon} from "../../../assets/svg/arrows.svg";
import {LoadingArrowsContainer} from "./styled";
import {useEffect, useState} from "react";
import {__getStylesState__} from "../../../common/globals";


export function LoadingWidget(
    props: Partial<{ active: boolean, onAnimationEnd: {(): void}, color: string, size: string }>
) {
    const [isActiveNow, setIsActiveNow]: [boolean|undefined, any] = useState(false);

    useEffect(() => {
        let deactivateSoon: ReturnType<typeof setTimeout>|undefined;

        if (!props.active) {
            deactivateSoon = setTimeout(
                () => {
                    if (isActiveNow) {
                        setIsActiveNow(false);
                        if (props.onAnimationEnd) {props.onAnimationEnd()}
                    }
                },
                500)
        } else {
            setIsActiveNow(true)
        }

        return () => {if (deactivateSoon) {clearTimeout(deactivateSoon)}}

    }, [props.active])

    return (<LoadingArrowsContainer size={props.size}>
        <ArrowsIcon
            style={{
                height: props.size ? props.size : "20px",
                width: props.size ? props.size : "20px",
                visibility: isActiveNow ? "visible" : "hidden"
            }}
            fill={props.color ? props.color : __getStylesState__().commonSecondBackgroundColor}
        />
    </LoadingArrowsContainer>)
}
