import styled, { keyframes } from "styled-components";


const rotateArrows = keyframes`
    from {
        transform: rotate(0deg);
    } to {
        transform: rotate(-360deg);
    }
`;


export const LoadingArrowsContainer = styled.div`
    animation: ${rotateArrows} 1s linear infinite;
    width: min-content;
    height: ${props => props.size ? props.size : "20px"};
    padding: 0;
    transform-origin: 50% 50%;
    display: inline-block;
`;
