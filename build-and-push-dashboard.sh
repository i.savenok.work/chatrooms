#!/bin/sh

# Docker must me logged in to gitlab registry!
# sudo docker login --username={gitlab username} --password={gitlab password} registry.gitlab.com

# first command argument is image tag
default_image_tag="main"

echo "--- Build and push dashboard to gitlab ---"

if [[ $1 = "" ]]
then
    image_tag=$default_image_tag
else
    image_tag=$1
fi
echo "Using image tag :$image_tag"

echo "Building image for dashboard..."
sudo docker build -t registry.gitlab.com/i.savenok.work/chatrooms/dashboard:"$image_tag" chatrooms-dashboard

echo "Pushing image to gitlab registry..."
sudo docker push registry.gitlab.com/i.savenok.work/chatrooms/dashboard:"$image_tag"

echo "Image is available by \"registry.gitlab.com/i.savenok.work/chatrooms/dashboard:$image_tag\""
