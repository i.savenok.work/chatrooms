import signal
import sys
from common.logging import info as _l
import asyncio
import websockets
import settings
from common.threads import Thread
from app import get_app
from entities.ws_connection.handlers import ws_handler


class AppThread(Thread):

    def run(self) -> None: get_app().run(host=settings.APP_HOST, port=settings.APP_PORT)


def terminate(sig, frame):
    _l('Terminating...')
    sys.exit(0)


if __name__ == '__main__':
    _l('Running...')
    signal.signal(signal.SIGINT, terminate)

    app_thread = AppThread()
    app_thread.setDaemon(True)
    app_thread.start()

    ws_server = websockets.serve(ws_handler, settings.WS_SERVER_HOST, settings.WS_SERVER_PORT)
    asyncio.get_event_loop().run_until_complete(ws_server)
    asyncio.get_event_loop().run_forever()
