from flask import jsonify
from flask.views import MethodView

from common.exceptions import Unauthorized
from common.flask import request


class View(MethodView):
    auth_required = False

    def dispatch_request(self, *args, **kwargs):
        if self.auth_required and not request.user:
            raise Unauthorized()
        return jsonify(super(View, self).dispatch_request(*args, **kwargs))


class HealthView(View):

    def get(self):
        return {'status': 'ok'}
