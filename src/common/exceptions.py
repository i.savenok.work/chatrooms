import json

from werkzeug import \
    exceptions as _exceptions  # noqa: werkzeug included in flask

GenericApiException = _exceptions.HTTPException


class MethodNotAllowed(_exceptions.MethodNotAllowed):
    pass


class BadRequest(_exceptions.BadRequest):
    pass


class Unauthorized(_exceptions.Unauthorized):
    pass


class NotFound(_exceptions.NotFound):
    pass


class Forbidden(_exceptions.Forbidden):
    pass


def handle_api_exception(e: GenericApiException):
    exc_data = {'details': getattr(e, 'description', getattr(e, 'name', 'Server error'))}

    response = e.get_response()
    response.data = json.dumps(exc_data)
    response.content_type = "application/json"
    return response
