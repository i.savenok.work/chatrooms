from common.utils import ThreadSafeSingleton, parse_get_params


def test_parse_get_params(fake):
    for raw, expected in [
        (fake.uri()+'?a=1&b=2&c=3', {'a': '1', 'b': '2', 'c': '3'}),
        (fake.word() + '?test=test', {'test': 'test'}),
        (fake.uri() + fake.word(), {}),
        (fake.word(), {}),
        ('', {}),
    ]:
        assert parse_get_params(raw) == expected


def test_singleton():

    class A(metaclass=ThreadSafeSingleton):
        def __init__(self, a, b, c=None): self.a, self.b, self.c = a, b, c

    a = A(a=1, b=2)
    assert isinstance(a, A)
    assert a.a == 1
    assert a.b == 2
    assert a.c is None

    a2 = A(a=1, b=2, c=3)
    assert isinstance(a2, A)
    assert a2.a == 1
    assert a2.b == 2
    assert a2.c is None

    assert a is a2
    assert id(a) == id(a2)

    a.a = 0
    assert a.a == a2.a == 0
