import uuid
from typing import Callable

import pytest
from faker import Faker
from faker.providers import BaseProvider

from run import get_app

faker = Faker()


class Provider(BaseProvider):
    word: Callable
    uri: Callable
    pyint: Callable
    sentence: Callable
    color: Callable
    name: Callable
    password: Callable

    @classmethod
    def hex_uuid(cls): return uuid.uuid4().hex


faker.add_provider(Provider)


@pytest.fixture(scope='session')
def fake() -> 'Provider':
    return faker  # noqa: typing


@pytest.fixture
def client():
    app = get_app()
    yield app.test_client()


class Equal:

    def __eq__(self, other): return True


@pytest.fixture
def equal(): return Equal()
