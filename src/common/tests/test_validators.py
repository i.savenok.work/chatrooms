from common.validators import is_valid_hex_color


def test_colors_validator(fake):
    assert is_valid_hex_color(fake.color())
    assert not is_valid_hex_color(fake.word())
    assert not is_valid_hex_color('')
    assert not is_valid_hex_color(fake.color()+'0')
