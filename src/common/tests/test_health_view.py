

def test_health_view(client):
    r = client.get('/health/')
    assert r.status_code == 200
    assert r.json == {'status': 'ok'}
