from threading import Lock
from typing import Dict
from urllib.parse import parse_qs, urlparse

lock = Lock()


class ThreadSafeSingleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            with lock:
                if cls not in cls._instances:
                    cls._instances[cls] = super(ThreadSafeSingleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


def parse_get_params(raw: str) -> Dict[str, str]:
    return {k: v[0] for k, v in parse_qs(urlparse(raw).query).items()}


def raise_if(condition: bool, details: str, exc_class: type = Exception):
    if condition:
        raise exc_class(details)
