import importlib

from flask import Flask as _FlaskApp
from flask import Request as _Request
from flask import request as _request
from flask_cors import CORS as _CORS
from werkzeug import wrappers as _wrappers

from settings import REQUEST_USER_GETTER


class Request(_Request):

    def __init__(self, environ: dict, *args, **kwargs):
        user_getter_module_name, user_getter_name = REQUEST_USER_GETTER.rsplit('.', 1)
        user_getter = getattr(importlib.import_module(user_getter_module_name), user_getter_name)
        self.user = user_getter(environ)
        super(_Request, self).__init__(environ, *args, **kwargs)


class FlaskApp(_FlaskApp):
    request_class = Request


request: Request = _request


class Response(_wrappers.Response):
    pass


class Request(_wrappers.Request):
    pass


CORS = _CORS
