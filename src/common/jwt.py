import jwt as _jwt

encode = _jwt.encode
decode = _jwt.decode


DecodeError = _jwt.DecodeError
