import re
import uuid


def is_valid_hex_color(raw: str) -> bool: return bool(re.fullmatch(r'^#(?:[0-9a-fA-F]{1,2}){3}$', raw))


def is_valid_uuid(raw: str) -> bool:
    try:
        uuid.UUID(raw)
        return True
    except Exception:  # noqa: safe
        return False
