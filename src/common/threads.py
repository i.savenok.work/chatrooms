import queue as _queue
import threading as _threading
from typing import Any, Coroutine, Dict, List

import janus as _janus

import settings
from common.utils import ThreadSafeSingleton


class Thread(_threading.Thread):
    pass


class WSEventsQueue(_janus.Queue, metaclass=ThreadSafeSingleton):

    """
        A special queue for messages from api to ws handler;
    put is sync, get is async.
    """

    def __new__(cls, *args, **kwargs):
        if settings.TEST_MODE:
            return SyncWSEventsQueue()
        return super(WSEventsQueue, cls).__new__(cls, *args, **kwargs)

    def put(self, item: Dict[str, Any], send_to: List[str] = None, send_to_room: str = None) -> None:
        return self.sync_q.put(self.format_item(item, send_to, send_to_room))

    @classmethod
    def format_item(cls, item: Dict[str, Any], send_to: List[str] = None, send_to_room: str = None) -> Dict[str, Any]:
        item = {'content': item}
        if send_to:
            item['send_to'] = send_to
        elif send_to_room:
            item['send_to_room'] = send_to_room
        return item

    def get(self) -> Coroutine: return self.async_q.get()

    def clear(self): raise NotImplementedError()


class SyncWSEventsQueue(_queue.Queue):  # for testing

    def put(self, item: Dict[str, Any], send_to: List[str] = None, send_to_room: str = None) -> None:  # noqa
        return super(SyncWSEventsQueue, self).put(WSEventsQueue.format_item(item, send_to, send_to_room))

    def clear(self): self.queue.clear()
