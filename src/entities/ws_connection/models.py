from typing import TYPE_CHECKING, Optional

if TYPE_CHECKING:
    from websockets.legacy.server import WebSocketServerProtocol
    from entities.user.models import User

from uuid import uuid4
from entities.ws_connection.events import ConnectionEvents


class Connection:
    ws: 'WebSocketServerProtocol'
    user: 'User'

    _current_room_id: Optional[str] = None

    id: str

    def __init__(self, ws: 'WebSocketServerProtocol', user: 'User', current_room_id: Optional[str] = None):
        self.ws, self.user, self.current_room_id = ws, user, current_room_id
        self.id = uuid4().hex

    @property
    def current_room_id(self) -> Optional[str]: return self._current_room_id

    @current_room_id.setter
    def current_room_id(self, value: Optional[str]):

        if value:
            ConnectionEvents.user_entered_room(user=self.user, room_id=value)
        if self._current_room_id:
            ConnectionEvents.user_left_room(user=self.user, room_id=self._current_room_id)

        self._current_room_id = value

    def after_unregistering(self):
        from entities.ws_connection.store import CONNECTION_STORE
        if self.current_room_id:
            """
                ws event "user left room" must be sent if user
            was connected to room and then left;  must be sent
            only  if there are no  other connections  to  that
            room by that user.
            """
            if not CONNECTION_STORE.is_user_in_room(user=self.user, room_id=self.current_room_id):
                ConnectionEvents.user_left_room(user=self.user, room_id=self.current_room_id)
