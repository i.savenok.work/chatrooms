from typing import TYPE_CHECKING, List, Iterator

if TYPE_CHECKING:
    from websockets.legacy.server import WebSocketServerProtocol
    from entities.user.models import User
from entities.ws_connection.models import Connection


class ConnectionsStore(dict):

    def __setitem__(self, key: str, value: Connection):
        return super(ConnectionsStore, self).__setitem__(key, value)

    def __getitem__(self, item: str) -> Connection: return super(ConnectionsStore, self).__getitem__(item)

    def register_connection(self, user: 'User', ws: 'WebSocketServerProtocol') -> Connection:
        connection = Connection(ws=ws, user=user, current_room_id=None)
        self[connection.id] = connection
        return connection

    def unregister_connection(self, connection: Connection):
        self.pop(connection.id, None)
        connection.after_unregistering()

    def filter_by_users(self, users_ids: List[str]) -> Iterator[Connection]:
        return filter(lambda conn: conn.user.id in users_ids, self.values())

    def filter_by_room(self, room_id: str) -> Iterator[Connection]:
        return filter(lambda conn: conn.current_room_id == room_id, self.values())

    def is_user_in_room(self, user: 'User', room_id: str) -> bool:
        try:
            next(filter(lambda conn: (conn.user.id == user.id) and (conn.current_room_id == room_id), self.values()))
            return True
        except StopIteration:
            return False


CONNECTION_STORE = ConnectionsStore()
