from typing import TYPE_CHECKING

from entities.user.views import serialize_user

if TYPE_CHECKING:
    from entities.user.models import User

from common.threads import WSEventsQueue


class ConnectionEvents:
    USER_ENTERED_ROOM = 'user.entered_room'
    USER_LEFT_ROOM = 'user.left_room'

    @classmethod
    def user_entered_room(cls, user: 'User', room_id: str):
        WSEventsQueue().put({'event': cls.USER_ENTERED_ROOM, 'data': {
            'user': serialize_user(user),
            'room_id': room_id,
        }}, send_to_room=room_id)

    @classmethod
    def user_left_room(cls, user: 'User', room_id: str):
        WSEventsQueue().put({'event': cls.USER_LEFT_ROOM, 'data': {
            'user_id': user.id,
            'room_id': room_id,
        }}, send_to_room=room_id)
