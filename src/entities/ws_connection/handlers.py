import json
from typing import TYPE_CHECKING

from common.logging import info as _l

if TYPE_CHECKING:
    from entities.ws_connection.models import Connection
    from websockets.legacy.server import WebSocketServerProtocol

import asyncio

import websockets

from common.threads import WSEventsQueue
from common.utils import parse_get_params
from entities.user.auth import InvalidToken, get_user_from_token
from entities.ws_connection.store import CONNECTION_STORE


class ClientEventType:
    USER_ENTERED_ROOM = 'user.entered_room'
    USER_LEFT_ROOM = 'user.left_room'


async def ws_handler_consumer(_connection: 'Connection'):
    try:
        async for message in _connection.ws:
            data = json.loads(message)
            event = data['event']

            if event == ClientEventType.USER_ENTERED_ROOM:
                _connection.current_room_id = data['data']['room_id']
            if event == ClientEventType.USER_LEFT_ROOM:
                _connection.current_room_id = None

    finally:
        _l(f'{_connection.user.name} closed ws connection.')
        CONNECTION_STORE.unregister_connection(_connection)


async def ws_handler_producer(_connection: 'Connection'):
    try:
        while True:
            event = await WSEventsQueue().get()
            if event and isinstance(event, dict):
                content = json.dumps(event['content'])

                send_to = event.get('send_to')
                send_to_room = event.get('send_to_room')
                if send_to:
                    recipients = CONNECTION_STORE.filter_by_users(send_to)
                elif send_to_room:
                    recipients = CONNECTION_STORE.filter_by_room(send_to_room)
                else:
                    recipients = CONNECTION_STORE.values()

                disconnected = []

                for target_connection in recipients:
                    try:
                        await target_connection.ws.send(content)
                    except websockets.ConnectionClosed:
                        disconnected.append(target_connection)

                for disconnected_connection in disconnected:
                    _l(f'{disconnected_connection.user.name} closed ws connection.')
                    CONNECTION_STORE.unregister_connection(disconnected_connection)

    finally:
        _l(f'Disconnecting {_connection.user.name} ws.')
        CONNECTION_STORE.unregister_connection(_connection)


async def ws_handler(ws: 'WebSocketServerProtocol', path: str):
    _l(f'Attempt to connect by ws with {path}')
    params = parse_get_params(path)
    try:
        connected_user = get_user_from_token(params.get('token'))  # todo not retrieve user in store, just parse id
    except InvalidToken:
        _l('Unauthorized ws connection.')
        await ws.close(reason='Unauthorized')
        return

    connection = CONNECTION_STORE.register_connection(user=connected_user, ws=ws)
    _l(f'{connected_user.name} connected by ws.')

    consumer_task = asyncio.ensure_future(ws_handler_consumer(connection))
    producer_task = asyncio.ensure_future(ws_handler_producer(connection))
    done, pending = await asyncio.wait([consumer_task, producer_task], return_when=asyncio.FIRST_COMPLETED)
    for task in pending:
        task.cancel()
