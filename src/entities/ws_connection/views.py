from common.views import View
from entities.user.store import USERS_STORE
from entities.room.store import ROOMS_STORE
from entities.ws_connection.store import CONNECTION_STORE


class WSDebugView(View):
    auth_required = False

    def get(self):
        return {
            'all_users': [u.name for u in USERS_STORE.values()],
            'all_rooms': [r.name for r in ROOMS_STORE.values()],
            'all_connections': {c_id: {
                'id': c.id,
                'user': c.user.name,
                'current_room': ROOMS_STORE[c.current_room_id].name if c.current_room_id else None
            } for c_id, c in CONNECTION_STORE.items()},
        }
