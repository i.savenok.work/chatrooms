from common.exceptions import BadRequest, NotFound, Forbidden
from common.flask import request
from common.views import View
from entities.room.models import Message, Room
from entities.room.store import ROOMS_STORE
from entities.user.views import serialize_user


def serialize_room(room: Room): return {'id': room.id, 'name': room.name}


def serialize_message(msg: Message): return {
    'id': msg.id,
    'content': msg.content,
    'user_id': msg.user.id,
    'room_id': msg.room.id,
    'user_name': msg.user.name,
    'order': msg.order,
}


class RoomsView(View):
    auth_required = True

    def post(self):
        data = request.json or {}
        name = str(data.get('name', ''))

        if not name:
            raise BadRequest('Name is required')
        if len(name) > 64:
            raise BadRequest('Max name length is 64')

        room = Room(name)
        ROOMS_STORE.save(room)

        return serialize_room(room)

    def get(self):
        return [serialize_room(room) for room in ROOMS_STORE.values()]


class MessagesView(View):
    auth_required = True

    def post(self, room_id: str = None):
        room = ROOMS_STORE.get(room_id)
        if not room:
            raise NotFound('Room does not exists')
        if not room.is_user_in_now(request.user):
            raise Forbidden('You must be connected to room to add messages')

        data = request.json or {}
        content = data.get('content')
        if not content:
            raise BadRequest('Content is required')
        if len(content) > 512:
            raise BadRequest('Max content length is 512')

        message = room.add_message(user=request.user, content=content)

        return serialize_message(message)

    def get(self, room_id: str = None):
        room = ROOMS_STORE.get(room_id)
        if not room:
            raise NotFound('Room does not exists')

        return [serialize_message(msg) for msg in room.messages]


class MembersView(View):
    auth_required = True

    def get(self, room_id: str = None):
        room = ROOMS_STORE.get(room_id)
        if not room:
            raise NotFound('Room does not exists')

        return [serialize_user(user) for user in room.members]


class WatchersView(View):
    auth_required = True

    def get(self, room_id: str = None):
        room = ROOMS_STORE.get(room_id)
        if not room:
            raise NotFound('Room does not exists')

        return [serialize_user(user) for user in room.get_watchers()]
