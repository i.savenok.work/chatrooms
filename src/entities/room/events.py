from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from entities.room.models import Room, Message

from common.threads import WSEventsQueue


class RoomsEvents:
    ROOM_CREATED = 'room.created'
    MESSAGE_CREATED = 'msg.created'

    @classmethod
    def room_created(cls, room: 'Room'):
        WSEventsQueue().put({'event': cls.ROOM_CREATED, 'data': {'id': room.id, 'name': room.name}})

    @classmethod
    def message_created(cls, message: 'Message'):
        WSEventsQueue().put({'event': cls.MESSAGE_CREATED, 'data': {
            'id': message.id,
            'room_id': message.room.id,
            'user_id': message.user.id,
            'user_name': message.user.name,
            'order': message.order,
            'content': message.content,
        }}, send_to_room=message.room.id)
