import pytest

from entities.room.models import Room
from entities.room.store import ROOMS_STORE
from entities.user.models import User


@pytest.fixture
def rooms_factory(fake):
    def create():
        room = Room(name=fake.name())
        ROOMS_STORE[room.id] = room
        return room
    return create


@pytest.fixture
def messages_factory(rooms_factory, users_factory, fake):
    def create(room: Room = None, user: User = None):
        room = room or rooms_factory()
        user = user or users_factory()
        msg = room.add_message(user=user, content=fake.sentence())
        return msg
    return create


@pytest.fixture
def room_watchers_adder(users_factory):
    def register(room: 'Room', user: 'User' = None):
        from entities.ws_connection.store import CONNECTION_STORE
        conn = CONNECTION_STORE.register_connection(ws='', user=user or users_factory())
        conn.current_room_id = room.id
        return conn.user
    return register
