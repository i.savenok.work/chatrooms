from common.threads import WSEventsQueue
from entities.room.events import RoomsEvents
from entities.room.store import ROOMS_STORE
from settings import API_PREFIX

PATH = F'{API_PREFIX}/rooms/'


class TestRoomApi:

    def test_add_room(self, client, fake, auth_head):
        name = fake.word()
        r = client.post(PATH, json={'name': name}, **auth_head)
        assert r.status_code == 200
        room = ROOMS_STORE[r.json['id']]
        assert room.name == name == r.json['name']

        msg = WSEventsQueue().get()
        assert msg == {
            'content': {
                'event': RoomsEvents.ROOM_CREATED,
                'data': {'id': room.id, 'name': room.name},
            }
        }

    def test_add_room_unauthorized(self, client, fake):
        r = client.post(PATH, json={'name': fake.word()})
        assert r.status_code == 401

    def test_add_room_without_data(self, client, auth_head):
        r = client.post(PATH, **auth_head)
        assert r.status_code == 400

    def test_add_room_without_name(self, client, auth_head):
        r = client.post(PATH, json={}, **auth_head)
        assert r.status_code == 400

    def test_add_room_with_invalid_name(self, client, auth_head):
        r = client.post(PATH, json={'name': 'a'*256}, **auth_head)
        assert r.status_code == 400

    def test_list_rooms(self, client, rooms_factory, auth_head):
        r = client.get(PATH, **auth_head)
        assert r.json == []

        r1, r2 = rooms_factory(), rooms_factory()
        r = client.get(PATH, **auth_head)
        assert isinstance(r.json, list)
        assert tuple(r.json) == ({'id': r1.id, 'name': r1.name}, {'id': r2.id, 'name': r2.name})

    def test_list_rooms_unauthenticated(self, client, rooms_factory):
        r = client.get(PATH)
        assert r.status_code == 401


class TestMessageApi:

    def path(self, room_id: str): return f'{PATH}{room_id}/messages/'

    def test_add_message(self, client, fake, rooms_factory, users_factory, get_auth_head, room_watchers_adder):
        room, content, user = rooms_factory(), fake.sentence(), users_factory()
        room_watchers_adder(room=room, user=user)
        users_factory()
        WSEventsQueue().clear()
        r = client.post(
            self.path(room.id),
            json={'content': content},
            **get_auth_head(user),
        )
        assert r.status_code == 200
        msg = room.messages[0]
        assert r.json['content'] == msg.content == content
        assert r.json['user_id'] == msg.user.id == user.id
        assert r.json['order'] == msg.order == 0

        ev = WSEventsQueue().get()
        assert ev == {
            'content': {
                'event': RoomsEvents.MESSAGE_CREATED,
                'data': {
                    'id': msg.id,
                    'room_id': msg.room.id,
                    'user_id': msg.user.id,
                    'user_name': msg.user.name,
                    'order': msg.order,
                    'content': msg.content,
                },
            },
            'send_to_room': msg.room.id,
        }

        content_2, user_2 = fake.sentence(), users_factory()
        room_watchers_adder(user=user_2, room=room)
        r = client.post(
            self.path(room.id), json={'content': content_2}, **get_auth_head(user_2)
        )
        msg = room.messages[-1]
        assert msg.content == content_2
        assert msg.user == user_2
        assert msg.order == 1

    def test_add_message_unauthorized(self, client, fake, rooms_factory):
        room, content = rooms_factory(), fake.sentence()
        r = client.post(
            self.path(room.id),
            json={'content': content},
        )
        assert r.status_code == 401

    def test_add_message_for_not_existing_room(self, client, fake, auth_head):
        r = client.post(self.path(fake.hex_uuid()), **auth_head)
        assert r.status_code == 404

    def test_add_message_by_not_connected_to_room_user(self, client, fake, rooms_factory, auth_head):
        r = client.post(
            self.path(rooms_factory().id),
            json={'content': fake.sentence()},
            **auth_head,
        )
        assert r.status_code == 403

    def test_list_messages(self, client, messages_factory, auth_head):
        msg_1 = messages_factory()
        msg_2 = messages_factory(room=msg_1.room)
        msg_3 = messages_factory(room=msg_1.room)
        messages_factory(), messages_factory(), messages_factory()

        r = client.get(self.path(msg_1.room.id), **auth_head)
        assert r.status_code == 200
        assert isinstance(r.json, list)
        assert len(r.json) == 3
        assert tuple(r.json) == (
            {
                'id': msg_1.id,
                'content': msg_1.content,
                'user_id': msg_1.user.id,
                'room_id': msg_1.room.id,
                'user_name': msg_1.user.name,
                'order': 0,
            },
            {
                'id': msg_2.id,
                'room_id': msg_2.room.id,
                'content': msg_2.content,
                'user_id': msg_2.user.id,
                'user_name': msg_2.user.name,
                'order': 1,
            },
            {
                'id': msg_3.id,
                'room_id': msg_3.room.id,
                'content': msg_3.content,
                'user_id': msg_3.user.id,
                'user_name': msg_3.user.name,
                'order': 2,
            },
        )


class TestMemberApi:

    def path(self, room_id: str): return f'{PATH}{room_id}/members/'

    def test_list_members(self, client, messages_factory, users_factory, auth_head):
        user_1, user_2, _ = users_factory(), users_factory(), users_factory()
        msg1 = messages_factory(user=user_1)
        messages_factory(room=msg1.room, user=user_2)
        messages_factory(room=msg1.room, user=user_2)
        messages_factory(room=msg1.room, user=user_2)

        r = client.get(self.path(msg1.room.id), **auth_head)
        assert r.status_code == 200
        assert tuple(r.json) == (
            {'id': user_1.id, 'name': user_1.name, 'color': user_1.color},
            {'id': user_2.id, 'name': user_2.name, 'color': user_2.color},
        )


class TestWatchersApi:

    def path(self, room_id: str): return f'{PATH}{room_id}/watchers/'

    def test_list_watchers(self, client, room_watchers_adder, rooms_factory, auth_head):
        room = rooms_factory()
        watcher_1, watcher_2 = room_watchers_adder(room), room_watchers_adder(room)
        room_watchers_adder(room=room, user=watcher_1)  # watcher_1 second connection, users in response must not repeat
        room_watchers_adder(rooms_factory())

        r = client.get(self.path(room.id), **auth_head)
        assert r.status_code == 200
        assert tuple(r.json) == (
            {'id': watcher_1.id, 'name': watcher_1.name, 'color': watcher_1.color},
            {'id': watcher_2.id, 'name': watcher_2.name, 'color': watcher_2.color},
        )
