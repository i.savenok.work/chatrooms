from dataclasses import dataclass, field
from typing import List
from uuid import uuid4

from entities.room.events import RoomsEvents
from entities.user.models import User


@dataclass
class Room:
    name: str
    messages: List['Message'] = field(default_factory=list)

    id: str = field(default_factory=lambda: uuid4().hex)

    @property
    def members(self) -> List[User]:
        results = []
        for msg in self.messages:
            if msg.user not in results:
                results.append(msg.user)
        return results

    def add_message(self, user: User, content: str) -> 'Message':
        msg = Message(user=user, content=content, order=len(self.messages), room=self)
        self.messages.append(msg)
        RoomsEvents.message_created(msg)
        return msg

    def get_watchers(self) -> List[User]:  # todo CONNECTION_STORE should be used only in ws handlers
        from entities.ws_connection.store import CONNECTION_STORE
        return list(set(c.user for c in CONNECTION_STORE.filter_by_room(room_id=self.id)))

    def is_user_in_now(self, user: 'User') -> bool:  # todo CONNECTION_STORE should be used only in ws handlers
        from entities.ws_connection.store import CONNECTION_STORE
        return CONNECTION_STORE.is_user_in_room(user=user, room_id=self.id)


@dataclass
class Message:
    room: Room
    user: User
    content: str
    order: int

    id: str = field(default_factory=lambda: uuid4().hex)
