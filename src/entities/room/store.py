from typing import Optional

from entities.room.events import RoomsEvents
from entities.room.models import Room


class RoomsStore(dict):

    def __setitem__(self, key: str, value: Room):
        return super(RoomsStore, self).__setitem__(key, value)

    def __getitem__(self, item: str) -> Room: return super(RoomsStore, self).__getitem__(item)

    def __contains__(self, item):
        if isinstance(item, Room):
            return item.id in self
        return super(RoomsStore, self).__contains__(item)

    def save(self, room: Room):
        self[room.id] = room
        RoomsEvents.room_created(room)

    def get(self, key: str) -> Optional[Room]: return super(RoomsStore, self).get(key)  # noqa: typing


ROOMS_STORE = RoomsStore()
