import pytest

from common import jwt
from common.validators import is_valid_uuid
from entities.user.auth import (
    InvalidToken,
    decode_token,
    encode_token,
    get_user_from_token
)
from entities.user.models import User
from entities.user.store import USERS_STORE
from settings import API_PREFIX, SECRET_KEY


class TestUser:

    def test_init(self, fake):
        name, color, password = fake.name(), fake.color(), fake.password()
        user = User(name=name, color=color, password=password)
        assert user.name == name
        assert user.color == color
        assert user.password == password
        assert is_valid_uuid(user.id)

    def test_identification(self, users_factory, fake):
        user = users_factory()
        user_id = user.id
        with pytest.raises(AttributeError):
            user.id = fake.hex_uuid()
        assert user.id == user_id

        user._User__id = fake.hex_uuid()
        assert user.id != user_id

    def test_equality(self, users_factory):
        user_1, user_2 = users_factory(), users_factory()
        assert user_1 != user_2
        assert tuple({user_1, user_2}) == (user_1, user_2)

        user_2._User__id = user_1.id
        assert user_1 == user_2
        assert tuple({user_1, user_2}) == (user_2,)


class TestUserStore:

    def test_add(self):
        user = User('', '', '')
        assert user not in USERS_STORE
        USERS_STORE.save(user)
        assert user in USERS_STORE
        assert USERS_STORE[user.id] == user

    def test_remove(self, users_factory, fake):
        user = users_factory()
        USERS_STORE.save(user)
        assert user in USERS_STORE
        USERS_STORE.remove(user)
        assert user not in USERS_STORE

        # by id
        USERS_STORE.save(user)
        assert user in USERS_STORE
        USERS_STORE.remove(user.id)
        assert user not in USERS_STORE

        # if not stored (must be safe)
        USERS_STORE.remove(fake.hex_uuid())
        USERS_STORE.remove(user)

    def test_get_by_name(self, users_factory):
        lookup_man = users_factory()
        users_factory(), users_factory(), users_factory()
        assert USERS_STORE.get_by_name(lookup_man.name) == lookup_man


class TestAuth:

    def test_encode_token(self, users_factory):
        user = users_factory()
        token = encode_token(user)
        assert isinstance(token, str)
        assert jwt.decode(jwt=token, key=SECRET_KEY, algorithms='HS256') == {
            'user_id': user.id,
        }

    def test_decode_token(self, fake, users_factory):
        user = users_factory()
        assert decode_token(encode_token(user)) == {'user_id': user.id}

        with pytest.raises(jwt.DecodeError):
            decode_token(fake.word())

        with pytest.raises(jwt.DecodeError):
            decode_token('')

        with pytest.raises(jwt.DecodeError):
            decode_token(None)

        with pytest.raises(jwt.DecodeError):
            decode_token(jwt.encode({}, key=fake.word(), algorithm='HS256'))

    def test_get_user_by_token(self, fake, users_factory):
        user = users_factory()
        assert get_user_from_token(encode_token(user)) == user

        USERS_STORE.remove(user)
        with pytest.raises(InvalidToken):
            get_user_from_token(encode_token(user))

        with pytest.raises(InvalidToken):
            get_user_from_token(fake.word())


PATH = f'{API_PREFIX}/users/'


class TestUserApi:

    def test_add(self, client, fake):
        name, color, password = fake.name(), fake.color(), fake.password()
        r = client.post(PATH, json={'name': name, 'color': color, 'password': password})
        assert r.status_code == 200
        user = USERS_STORE[r.json['id']]
        assert r.json['name'] == name == user.name
        assert r.json['color'] == color == user.color
        assert decode_token(r.json['token']) == {'user_id': user.id}
        assert 'password' not in r.json
        assert password == user.password

    def test_add_with_already_existing_name(self, client, fake, users_factory):
        user = users_factory()
        r = client.post(PATH, json={'name': user.name, 'color': fake.color()})
        assert r.status_code == 400

    def test_login(self, client, fake, users_factory):
        user = users_factory()
        users_factory(), users_factory()
        r = client.post(PATH, json={'name': user.name, 'password': user.password})
        assert r.status_code == 200
        assert r.json['id'] == user.id
        assert decode_token(r.json['token']) == {'user_id': user.id}

    def test_add_with_invalid_color(self, client, fake):
        r = client.post(PATH, json={'name': fake.name(), 'color': fake.word()})
        assert r.status_code == 400

    def test_try_to_add_with_empty_data(self, client):
        r = client.post(PATH)
        assert r.status_code == 400

    def test_try_to_add_without_color(self, client, fake):
        r = client.post(PATH, json={'name': fake.name()})
        assert r.status_code == 400

    def test_get_self_profile(self, client, users_factory, get_auth_head, equal):
        user = users_factory()
        r = client.get(PATH+'me/', **get_auth_head(user))
        assert r.status_code == 200
        assert r.json == {
            'color': user.color,
            'id': user.id,
            'name': user.name,
            'token': equal,
        }
        assert get_user_from_token(r.json['token']) == user

    def test_get_self_profile_unauthenticated(self, client, get_auth_head):
        r = client.get(PATH+'me/')
        assert r.status_code == 401

        user = User('', '', '')
        r = client.get(PATH+'me/', **get_auth_head(user))
        assert r.status_code == 401

    def test_get_user_info(self, client, auth_head, users_factory):
        user = users_factory()
        r = client.get(PATH+user.id+'/', **auth_head)
        assert r.status_code == 200
        assert r.json == {
            'id': user.id,
            'name': user.name,
            'color': user.color,
        }

    def test_get_user_info_unauthenticated(self, client, users_factory):
        user = users_factory()
        r = client.get(PATH+user.id+'/')
        assert r.status_code == 401

    def test_get_not_existing_user_info(self, client, auth_head, fake):
        r = client.get(PATH+fake.hex_uuid()+'/', **auth_head)
        assert r.status_code == 404
