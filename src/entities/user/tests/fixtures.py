import pytest

from entities.user.auth import encode_token
from entities.user.models import User
from entities.user.store import USERS_STORE


@pytest.fixture
def users_factory(fake):
    def create(name: str = None):
        user = User(
            name=name or fake.name(),
            color=fake.color(),
            password=fake.password(),
        )
        USERS_STORE[user.id] = user
        return user
    return create


@pytest.fixture
def auth_head(users_factory):
    return {'headers': {'Authorization': f'JWT {encode_token(users_factory())}'}}


@pytest.fixture
def get_auth_head():
    def gen(user):
        return {'headers': {'Authorization': f'JWT {encode_token(user)}'}}
    return gen
