from dataclasses import dataclass, field
from uuid import uuid4


@dataclass
class User:
    name: str
    color: str
    password: str

    __id: str = field(default_factory=lambda: uuid4().hex)

    @property
    def id(self): return self.__id

    def __eq__(self, other) -> bool:
        if isinstance(other, User):
            return other.id == self.id
        return False

    def __hash__(self): return hash((self.id,))
