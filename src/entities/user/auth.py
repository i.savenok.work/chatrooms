from typing import Optional

from common import jwt
from entities.user.models import User
from entities.user.store import USERS_STORE
from settings import SECRET_KEY


class InvalidToken(Exception):
    message = 'Invalid token'


def encode_token(user: User) -> str: return jwt.encode(payload={'user_id': user.id}, key=SECRET_KEY)


def decode_token(raw: str) -> dict: return jwt.decode(jwt=raw, key=SECRET_KEY, algorithms='HS256')


def get_user_from_token(raw: str) -> User:
    try:
        decoded = decode_token(raw)
        user_id = decoded['user_id']
        return USERS_STORE[user_id]
    except Exception:  # noqa safe
        raise InvalidToken()


def get_user_from_request(context: dict) -> Optional[User]:
    try:
        return get_user_from_token(context.get('HTTP_AUTHORIZATION', '')[4:])
    except InvalidToken:
        return
