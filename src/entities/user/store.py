from typing import Optional, Union

from entities.user.models import User


class UsersStore(dict):

    def __setitem__(self, key: str, value: User):
        return super(UsersStore, self).__setitem__(key, value)

    def __getitem__(self, item: str) -> User: return super(UsersStore, self).__getitem__(item)

    def get_by_name(self, name: str) -> Optional[User]:
        try:
            return filter(lambda u: u.name == name, self.values()).__next__()
        except StopIteration:
            return

    def save(self, user: User): self[user.id] = user

    def remove(self, user: Union[User, str]): self.pop(getattr(user, 'id', user), None)  # user is user or user id

    def __contains__(self, item):
        if isinstance(item, User):
            return item.id in self
        return super(UsersStore, self).__contains__(item)


USERS_STORE = UsersStore()
