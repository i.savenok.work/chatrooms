from common.exceptions import BadRequest, NotFound
from common.flask import request
from common.validators import is_valid_hex_color
from common.views import View
from entities.user.auth import encode_token
from entities.user.models import User
from entities.user.store import USERS_STORE


def serialize_user(user: User) -> dict: return {
    'id': user.id,
    'name': user.name,
    'color': user.color,
}


def _serialize_user_private(user: User) -> dict: return {
    'id': user.id,
    'name': user.name,
    'color': user.color,
    'token': encode_token(user),
}


class UsersView(View):

    def post(self):
        data = request.json or {}
        name, color, password = str(data.get('name')), str(data.get('color')), str(data.get('password'))

        if not name:
            raise BadRequest('Name is required')
        if not password:
            raise BadRequest('Password is required')

        existing_user = USERS_STORE.get_by_name(name)
        if existing_user:
            if existing_user.password == password:
                return _serialize_user_private(existing_user)
            else:
                raise BadRequest(f'Wrong password for {existing_user.name}')

        else:
            if not color:
                raise BadRequest('Color is required')
            if not is_valid_hex_color(color):
                raise BadRequest('Color must be a valid hex color')
            if len(name) > 64:
                raise BadRequest('Max name length is 64')
            if len(password) > 64:
                raise BadRequest('Max password length is 64')
            user = User(name=name, color=color, password=password)
            USERS_STORE.save(user)
            return _serialize_user_private(user)


class UsersDetailedView(View):
    auth_required = True

    def get(self, user_id: str):
        if user_id == 'me':
            return _serialize_user_private(request.user)

        user = USERS_STORE.get(user_id)
        if user is None:
            raise NotFound('User does not exists')
        return serialize_user(user)
