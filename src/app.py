import settings
from common.exceptions import GenericApiException, handle_api_exception
from common.flask import CORS, FlaskApp
from common.views import HealthView
from entities.room.views import MembersView, MessagesView, RoomsView, WatchersView
from entities.user.store import USERS_STORE
from entities.user.views import UsersDetailedView, UsersView
from entities.ws_connection.views import WSDebugView


def get_app() -> FlaskApp:
    app = FlaskApp(__name__)
    app.register_error_handler(GenericApiException, handle_api_exception)  # noqa its ok
    CORS(app)

    if settings.DEBUG:
        app.add_url_rule('/debug/', view_func=WSDebugView.as_view('ws-debug'))
    app.add_url_rule('/health/', view_func=HealthView.as_view('health'))
    pref = settings.API_PREFIX
    app.add_url_rule(f'{pref}/users/', view_func=UsersView.as_view('users'))
    app.add_url_rule(f'{pref}/users/<string:user_id>/', view_func=UsersDetailedView.as_view('users-det'))
    app.add_url_rule(f'{pref}/rooms/', view_func=RoomsView.as_view('rooms'))
    app.add_url_rule(f'{pref}/rooms/<string:room_id>/messages/', view_func=MessagesView.as_view('messages'))
    app.add_url_rule(f'{pref}/rooms/<string:room_id>/members/', view_func=MembersView.as_view('members'))
    app.add_url_rule(f'{pref}/rooms/<string:room_id>/watchers/', view_func=WatchersView.as_view('watchers'))

    return app
