import os

from common.logging import INFO, basicConfig

basicConfig(
    level=INFO,
    format="%(message)s",
)


APP_HOST = os.getenv('APP_HOST', '127.0.0.1')
APP_PORT = os.getenv('APP_PORT', 8000)

WS_SERVER_HOST = os.getenv('WS_SERVER_HOST', '127.0.0.1')
WS_SERVER_PORT = os.getenv('WS_SERVER_PORT', 5678)

API_PREFIX = os.getenv('API_PREFIX', '/api/v1')
SECRET_KEY = os.getenv('SECRET_KEY', 'secret')

REQUEST_USER_GETTER = 'entities.user.auth.get_user_from_request'

DEBUG = os.getenv('DEBUG_MODE', True)
TEST_MODE = os.getenv('TEST_MODE', False)
