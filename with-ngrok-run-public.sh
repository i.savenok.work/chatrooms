#!/bin/sh

DEPLOYMENT_FILE_NAME="docker-compose.dev.yml"
PROBE_TIMEOUT=60

log_h() {
    COLUMNS=$(tput cols)
    echo ""
    printf "%*s\n" $(((${#1}+$COLUMNS)/2)) "========= $1 ========="
}
log() {
    echo ""
    echo "$1"
}
cleanup() {
    log_h "Exiting"

    log "Stopping cluster..."
    sudo docker-compose stop

    log "Killing ngrok tunnel if exists..."
    killall ngrok

    log "Removing temporary files..."
    rm $DEPLOYMENT_FILE_NAME
    rm "ngrok.log"

    log "Suiciding..."
    trap - INT
    kill -s INT "$$"
}
trap cleanup INT TERM


log_h "Running Chatrooms app cluster"


log_h "1. Running Ngrok proxy"
# run ngrok tunnel
ngrok http 8080 > /dev/null &
# waiting for ngrok tunnel initialisation
while ! nc -z localhost 4040; do
    log "Waiting for ngrok tunnel initialisation..."
    sleep 1
done
# request ngrok proxy state
DATA=$(curl http://localhost:4040/api/tunnels)
# parse public url
PUBLIC_HOST=$(echo "$DATA" | sed -e 's/.*\"public_url\":\"https:\/\/\([a-zA-Z.0-9]*\)\",\"proto.*/\1/')
PUBLIC_HTTP_HOST="http://$PUBLIC_HOST"
PUBLIC_WS_HOST="ws://$PUBLIC_HOST"
log "Public host is: $PUBLIC_HTTP_HOST."


log_h "2. Creating deployment file"
# copying docker-compose.yml as template
cp docker-compose.yml $DEPLOYMENT_FILE_NAME
# setting environment variables
set_env() {
    log "Setting env REACT_APP_API_HOST as $2"
    sed -i -e "s~^\(\s*- $1\s*=\s*\).*~\1$2~" $DEPLOYMENT_FILE_NAME
}
set_env "REACT_APP_API_HOST" "$PUBLIC_HTTP_HOST"
set_env "REACT_APP_API_PORT" "none"
set_env "REACT_APP_WS_HOST" "$PUBLIC_WS_HOST"
set_env "REACT_APP_WS_PORT" "none"


log_h "3. Running cluster"
sudo docker-compose -f $DEPLOYMENT_FILE_NAME up -d

SLEEP_TIME=5
ATTEMPTS_COUNT=$((PROBE_TIMEOUT/SLEEP_TIME))
for i in $(seq $ATTEMPTS_COUNT); do
    log "Listening health, wait..."
    if [ "$(curl -s -o /dev/null -w "%{http_code}" "$PUBLIC_HTTP_HOST")" = "200" ]; then

        log_h "Chatrooms cluster is running"
        log_h "Tunnel uri: $PUBLIC_HTTP_HOST"
        cat
    fi
    sleep $SLEEP_TIME
done

log_h "Еhe cluster did not rise in the allotted time "
log "Check logs with \"sudo docker-compose logs -f\""
log "Check api health on $PUBLIC_HTTP_HOST/health/"
log "Check tunnel status on http://localhost:4040"
log "Press Ctrl+C to shut down"
cat
